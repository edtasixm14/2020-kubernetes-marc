# Projecte d'introducció a Kubernetes

![](../aux/kubernetes-logo.png)

                                Autor: Marc Gómez Cardona 

                                Curs: 2019/2020

                                EDT ASIX

# 

# Què és un contàiner?

Els **contàiners** són (mètodes) centrats en les aplicacions per a una resposta d'alt rendiment, aplicacions escalables...

Són perfectes per l'execució de microserveis per la seva portabilitat, l'execució en entorns virtuals aïllats sense que la seva execució es vegi interferida per altres aplicacions executant-se.

Els **microserveis** són aplicacions lleugeres (fetes en varis llengüatges) amb unes dependències, llibreries i entorn necessàris. Per a assegurar-nos de que s'executaràn de forma correcte les encapsulem junt amb les seves dependències.

Els **contàiners** encapsulen aquests microserveis i les seves dependències, però **no** els executen. Per a això tenim les **imatges de contàiners**.

Les **imatges de contàiners** empaqueten l'aplicació junt amb la seva execució i dependències necessàries, i amb el contàiner llençem aquesta imatge.

# Què és un Container Orchestrator?

El desplegament de contàiners en una sola màquina ens pot resultar útil per fer proves o veure com funciona els serveis que tenim, però això deixa de ser òptim quan passem a un nivell de producció on necessitem vàries (o moltes) màquines oferint el mateix servei. Tot això necessita els següents requeriments:

* Tolerància als errors/caigudes

* Escalabilitat segons la demanda

* Nivell òptim d'usatge dels recursos

* Que els contàiners es puguin comunicar de forma automàtica entre ells

* Accessibilitat des de qualsevol lloc del món

* Capacitat d'actualitzacions/rollbacks sense apagar el servei.

Per a controlar i monitoritzar això fem servir els anomenats **orquestradors**.

Un **Container Orchestrator** (*orquestrador de contàiners*)  és una eina on podem organitzar, monitoritzar i gestionar (entre altres) els nostres contàiners.

Tot i que podem fer un manteniment manual d'un conjunt de contàiners, o utilitzar scripts, la veritat és que els orquestradors facilitzen molt aquestes accions, especialment quan s'han de gestionar cents o milers de contàiners executant-se en una infraestructura global.

La majoria d'orquestradors poden:

* Agrupació de hosts alhora que crean un cluster.

* Programar l'execució de contàiners en hosts segons els recursos disponibles.

* Habilitar que els contàiners d'un cluster puguin comunicar-se entre ells independenment del host on estiguin desplegats.

* Lligar els contàiners i els recursos d'emmagatzematge.

* Agrupar sets de contàiners similars i lligar-los a construccions balançejadores de càrrega per a aplicacions/serveis.

* Administrar i optimitzar l'usatge dels recursos.

* Habilitar l'implementació de polítiques d'accés a les aplicacions/serveis executant-se als contàiners.

Alguns exemples d'orquestradors són:

* `Amazon Elastic Container Service (ECS)`: servei hostejat per **Amazon Web Services (AWS)** per a l'execució de contàiners escalats a la seva infraestructura.

* `Azure Container Instances (ACI)`: Orquestrador bàsic proveït per **Microsoft Azure**.

* `Azure Service Fabric`: Orquestrador open source proveït per **Microsoft Azure**.

* `Kubernetes`: Orquestrador open source, iniciat per Google, part del projecte **Cloud Native Computing Foundation (CNCF)**.

* `Marathon`: Framework per executar contàiners en escala a **Apache Mesos**.

* `Nomad`: Orquestrador proveït per **HashiCorp**.

* `Docker Swarm`: Orquestrador proveït per **Docker, Inc**. Format part del **Docker Engine**.

# Què és Kubernetes?

**Kubernetes** és un sistema open source fer al desplegament automàtic, escalat i administració de contàiners.  El nom prové del grec, que es pot traduïr com "*pilot de vaixell*", d'aquí el seu logo. També se'l coneix per **k8s**, ja que hi han 8 caràcters entre la *k* i la *s*.

**Kubernetes** està molt inspirat pel sistema **Borg** de Google, un orquestrador per a operacions globals durant més d'una decada. Kubernetes va ser iniciat per Google i, amb la seva v1.0 llençada al Juliol del 2015, la va donar a la *Cloud Native Computing Foundation*.

Està escrit en el llengüatge **Go** i llicenciat amb l'*Apache License V2*.

# Característiques de Kubernetes

Kubernetes ofereix un gran número de característiques per a l'orquestració de contàiners, algunes d'aquestes són:

* `Empaquetat automàtic`: Kubernetes organitza de forma automàtica els contàiners basant-se en les necessitats i restriccions dels recursos, maximitzant la seva utilització sense sacrificar la disponibilitat.

* `Self-healing`: Kubernetes substitueix i rellença de forma automàtica els contàiners dels nodes fallits. Mata i torna a executar els contàiners que no responen a *health checks*, basat en regles/polítiques existents. També prevé que el tràfic vaig cap a contàiners que no responen.

* `Escalatge horitzontal`: Amb Kubernetes les aplicacions passen d'estar escalades manualment a automàticament basat en mètriques de la CPU o altres d'utilització dessignades.

* `Service discovery i balanceig de càrrega`: Els contàiners reben la seva **pròpia IP** de Kubernetes, mentres que assigna un nom de domini per a aquest conjunt per ajudar al balanceig de càrrega entre els contàiners.

* `Rollouts i rollbacks automatitzats`: Kubernetes realitza sense problema rolls out i rolls back de les actualitazions i canvis de configuració de les aplicacions/serveis, monitoritzant constantment la salut d'aquests per a prevenir qualsevol caiguda de servei.

* `Secrets i gestió de la configuració`: Kubernetes gestiona secrets i detalls de la configuració de cada aplicació separat de l'imatge del contàiner, per a prevenir una reconstrucció de la imatge. Els secrets consisteixen en informació confidencial passada a l'aplicació sense revelar contingut sensible a la pila de configuració.

* `Orquestració d'emmagatzematge`: Kubernetes monta automàticament solucions **software-defined storage (SDS)** als contàiners des de l'emmagatzematge loca, proveïdors externs de cloud o sistemes d'emmagatzematge de xarxa.

* `Execució per lots`: Kubernetes admet execució per lots, feines de llarga durada i substitueix contàiners fallits.

# Per què Kubernetes?

Per què ens decantem per Kubernetes? Afegint a totes les característiques que té, Kubernetes també és portable i extensible. Es pot desplegar en molts entorns tant locals com remots, màquines virtuals... També admet i és admés per moltes eines open source de tercers que milloren la capacitat de Kubernetes i proveeixen una experiéncia plena per als usuaris.

L'arquitectura de Kubernetes és modular i pluggable, aixi la seva funcionabilitat es pot ampliar amb recursos personalitzats, operadors, APIs pròpies, regles programades o plugins.

A tot això se li afegeix la gran comunitat que hi té al darrere, tenint més de 2.000 col·laboradors que han fet més de 77.000 commits.

També podem trobar l'èxit de Kubernetes amb algunes empreses de renom com són:

* eBay

* Huawei

* IBM

* ING

* Nokia

* ...

# Arquitectura de Kubernetes

A un nivell alt, Kubernetes té els següents components principals:

* Un o més **master** nodes.

* Un o més **worker** nodes.

* Una clau-valor distribuida, tal com **etcd**.
  
  ![Estructura de Kubernetes del curs d'EDX](../aux/Kubernetes_Architecture.png)

                                                                                                        *imatge del curs d'edx* 

## Master node (node mestre)

El node **mestre** proveeix el panell de control responsable de monitoritzar l'estat del cluster i és el cervell darrere de totes les operacions que passin al cluster.

Els usuaris enviaràn peticions al node mestre ja sigui per CLI (*Command Line Interface*), Web UI (*Web User Interface*) o una API (*Application Programming Interface*).

És molt important mantindre aquest panell executant-se a qualsevol cost, perdre'l podria causar tallades del servei, interrupcions amb els clients... Per a assegurar que això no passi, tenim rèpliques del node mestre configurades com a *High-Availability*.  Així, si perdem el node mestre principal, el panell es seguirà sincronitzant amb les rèpliques i no el perdrem.

Per a desar l'estat del clúster, totes les dades de la configuració del clúster es guarden a **etcd**. Tot i això, **etcd** és una clau-valor distribuida que només desa l'estat del clúster, no les dades de càrrega dels clients. 

**etcd** es configura al node mestre (*stacked*) o en un host dedicat (*external*) per a reduïr les probabilitats de la pèrduda de dades. Quan és el primer cas, les rèpliques del node mestre asseguren també la resilència de l'etcd.

### Components del node mestre

En el node mestre tenim els següents components:

* `El servidor API`: Totes les tasques administratives són coordinades per **kube-apiserver**, un component del panell de control central al node mestre.
  
  El servidor intercepta les peticions (?) *RESTful*  dels usuaris, operadors i agents externs, per aprobar-los i processar-los. Durant aquest procés el servidor llegeix l'estat actual del clúster a *etcd* i, després d'una execució, el estat resultant el desa de nou.
  
  El servidor API és l'únic component del node mestre capaç de comunicar-se amb el magatzem de dades de l'*etcd*, tant per llegir com per guardar-hi l'estat del clúster, actuant com a middle-man (?) per a qualsevol altre agent que requereixi accedir al magatzem de dades.
  
  És altament configurable i customizable.

* `Planificador`: El rol del **kube-scheduler** és el d'assignar nous objectes, tal com *pods* (?), als nodes. Durant el procés de planificació, les dessicions es prenen en base a l'estat actual del clúster i dels requeriments del nou objecte. Obté del *etcd*, a través del *servidor API*, les dades que necessita cada *node treballador* al clúster. També rep, de part del servidor API, els requereiments dels nous objectes els quals són part de les seves dades de configuració.
  
  Els requeriments poden incloure restriccions que els usuaris i els operadors estableixen, tal com la planificació del treball en un node etiquetat com a **disk==ssd** a la seva clau/valor.
  
  És altament configurable i customizable, sent molt important i complexe en un clúster multi-node.

* `Controller managers`: És el component que s'executa en el node mestre que regula el estat del clúster, comparant constantment l'estat dessitjat (detallats a les dades de configuració dels objectes) amb l'actual. En el cas de que siguin diferents s'apliquen accions correctives fins que l'estat sigui el dessitjat.
  
  El **kube-controller-manager** executa controladors responsables d'actuar quan els nodes fallen per assegurar que els pods (?) són els esperats, crear endpoints, comptes de servei i tokens d'accés a l'API.
  
  El **cloud-controller-manager** és el responsable d'interactuar amb la infraestructura subjacent d'un proveïdor de cloud quan un node falla, administra els volums d'emmagatzematge proveïts pel servei de cloud i administra tant el balanceig de càrrega com l'enrutament. 

* `etcd`: **etcd** és un magatzem de dades clau-valor distribuït que es fa servir per a desar l'estat del clúster. Les noves dades s'escriuen afegint-les de nou, mai substituint-les. Les dades obsoletes es comprimeixen de forma periòdica per a minimitzar el tamany del magatzem de dades. Només s'hi pot comunicar el **servidor API**.
  
  Les eines d'administració permeten fer backups, snapshots i retaurar capacitats que són útils especialment per una sola instància d'*etcd* en un clúster de Kubernetes. Tot i això, si ens trobem en entorns de *Stage* o *Production* és extremadament important tindre rèpliques en mode d'alta disponibilitat per a la resilència de les dades. 

## Worker node (node treballador)

El **node treballador** executa l'entorn per a les aplicacions clients. Tot i que són microserveis containeritzats, aquestes aplicacions s'encapsulen en *Pods*, controlats per agents del panell de control del clúster executant-se al node *mestre*.

L'execució dels *Pods* es programa en els nodes *treballador*, on tenen les característiques físiques necessàries (cpu, memoria, discs...) i una xarxa on parlen entre ells i el món exterior.

Un *Pod* és la únitat mínima en la que es programa a Kubernetes. És una colecció de un o més contàiners programats junts.

Per a accedir a les aplicacions des de fora, connectem als nodes *treballador* i no al node *mestre*.

![Arquitectura d'un node treballador per EDX](../aux/worker_node.png)

                                                                       *Imatge del curs d'edx*

### Components del node treballador

El node *treballador* té els següents components:

* `Container runtime`: Tot i que Kubernetes es descriu com un "motor d'orquestració de contàiners" no té la capacitat de gestionar els contàiners directament. Per tal d'executar i administrar el cicle de vida d'un contàiner, Kubernetes necessita el **container runtime** en el node *treballador* on els *Pods* i els contàiners s'executaràn. Kubernetes suporta molts container runtimes:
  
  * `Docker`: Docker fa servir **containerd** com a container runtime. És el més usat amb Kubernetes.
  
  * `CRI-O`: Un container runtime lleuger per a Kubernetes, també suporta registres d'imatges de Docker.
  
  * `containerd`: Un container runtime portable i simple que aporta robustesa.
  
  * `rkt`: Un motor de *pods* natiu i contàiners, també executa imatges Docker.
  
  * `rktlet`: El **Container Runtime Interface (CRI)** de Kubernetes amb *rkt*.

* `kubelet`: És l'agent executant-se a cada node *treballador* que comunica als components del node *mestre*. Rep les definicions dels *Pods* (principalment des del servidor API), e interactua amb el *container runtime* al node *treballador* per executar els contàiners associats amb el *Pod*. També monitoritza la salut dels contàiners executant-se als *Pods*.
  
  El **kubelet** connecta amb el container runtime fent server un **CRI**. Aquest constisteix en buffers de protocol, gRPC API i llibreries.
  
  El **CRI** implementa dos serveis:
  
  * `ImageService`: És el responsable de totes les operacions relaciones amb les imatges.
  
  * `RuntimeService`: És el responsable de totes les operacions relaciones amb el contàiners i el *Pod*.

* `kube-proxy`: És l'agent de xarxa encarregat de les actualitzacions dinàmiques de manteniment de totes les regles de xarxa al node. Resumeix els detalls de xarxa dels *Pods* i reenvia les peticions de connexió als *Pods*.

* `Addons`: Els addons són característiques i funcionalitats que encara no estàn disponibles a Kubernetes, però que es poden implementar a través de tercers:
  
  * `DNS`: Un clúster DNS és un servidor DNS requerit per a assignar registres DNS als objectes i recursos de Kubernetes.
  
  * `Dashborad`: Una interfície d'usuari basada en web amb finalitats de l'administració del clúster.
  
  * `Monitoring`: Recull mètriques del contàiner al clúster i les desa en un magatzem de dades central.
  
  * `Logging`: Reculls els logs del contàiner al clúster i les desa en un magatzem de logs central per al seu anàlisi.

# Configuració e instal·lació de Kubernetes

Podem instal·lar Kubernetes fent servir diferents configuracions. Els quatre tipus d'instal·lacions són:

* `All-in-One Single-Node Installation`: En aquesta configuració, tots els components tant del node mestre com del treballador són instal·lats i executats en un únic node. És útil per aprendre, desenvolupament i testing, però no s'ha de fer servir a producció. Un exemple és **Minikube**.

* `Single-Node etcd, Single-Master and Multi-Worker Installation`: En aquesta configuració tenim un únic node mestre, que també executa una única instància d'etcd. Múltiples nodes treballadors es connecten al node mestre.

* `Single-Node etcd, Multi-Master and Multi-Worker Installation`: En aquesta configuració tenim varis nodes mestre configurats en mode HA (*High Availability*), però sol un node amb una instància d'etcd. Múltiples nodes treballadors es connecten al nodes mestre.

* `Multi-Node etcd, Multi-Master and Multi-Worker Installation`: En aquesta configuració tenim etcd configurat en un clúster en mode HA, els nodes mestre també estàn configurats en mode HA i els nodes treballadors estàn connectats a aquests. Aquesta és la configuració més avançada i recomenada per a configuracions de producció.

## Eines/Recursos útils

Tenim varies eines/recursos útils per a l'instal·lació o administració de clústers:

* `kubeadm`: És una forma segura i recomenada per a l'arrencada tant de clústers en mode single o multi node. Té un conjunt de blocs per a configurar el clúster, però és molt senzill d'extendre afegint-li més característiques.
  
  Kubeadm no suporta l'aprovisionament de hosts.

* `kubespray`: Amb *kubespray* (també conegut com *kargo*) podem instal·lar clústers HA (*Highly Available*) a ***AWS***, ***GCE***, ***Azure***, ***OpenStack*** o ***localment***.
  
  Està basat en *Ansible* i està disponible a la majoria de distribucions de Linux.

* `kops`: Amb *kops* podem crear, destruir, actualitzar, i mantenir el grau de producció, clústers HA (*Highly Available*) des de la línia de comandes. També pot subministrar les màquines.
  
  Actualment, ***AWS*** és oficialment suportat, mentres que el suport per a ***CGE*** es troba en beta. ***VMware vSphere*** es troba en una versió alpha i altres plataformes estàn planejades per al futur.

* `kube-aws`: Amb *kube-aws* podem crear, actualitzar i destruir clústers a ***AWS*** des de la línia de comandes.

## Minikube

*Minikube* és la forma més senzilla i recomanada per a arrencar clústers *all in one* de forma local a les nostres màquines. S'instal·la directament en un Linux, macOS o Windows local. Tot i això, i per a aprofitar totes les aventatges que ofereix *Minikube*, s'hauria d'instal·lar un **type-2 Hypervisor**, per a executar-se en conjunt amb Minikube. El que fa Minikube amb l'hipervisor és invocar-lo i crear una VM on executarà el clúster single node. 

Requeriments per a executar *Minikube*:

* `kubectl`: *kubectl* és un binari que accedeix i administra qualsevol clúster de Kubernetes. És necessaria la seva instal·lació per a poguer administrar el clúster.

* `Type-2 Hypervisor`: Per a Linux poden ser **VirtualBox** o **KVM**. Hem d'assegurar-nos de que tenim l'opció de virtualització habilitada a la nostra BIOS (*VT-x* o *AMD-v* depenent de la nostra CPU)
  
  * Tot i això, *Minikube* suporta l'opció `--vm-driver=none` que executa el components de Kubernetes directament al sistema local i no a una VM. Per a aquesta opció és necessari tindre instal·lat **Docker**, per lo que no necessitem l'instal·lació d'un hipervisor. Si fem servir aquesta opció ens hem d'assegurar d'especificar una *bridge network* (?) per a Docker, si no pot canviar entre reinicis de xarxa, cuasant la pèrdua de connectivitat al clúster.

* Conexió a internet la primera execució de *Minikubes* per a la descàrrega de paquets, dependèndices, actualitzacions i descàrrega d'imatges necessàries.

Mentres engegem Minikube el procés de startup crea, per defecte, un fitxer *config* dins del directori ***.kube*** del home de l'usuari (referenciat també com a ***dot-kube-config***). 

* Hem de tindre en compte que altres eines no creen aquest fitxer de forma automàtica i l'hem de crear nosaltres de forma manual. També que potser necessitem reconfigurar per adaptar-se a diverses xarxes i clients/server setups.

Aquest fitxer de configuració conté tots els detalls de la connexió requerits per *kubectl*. Per defecte, el binari de *kubectl* analitza aquest fitxer per trobar la connexió del node mestre i les credencials.

Podem veure el contingut d'aquest fitxer al inspeccionar-lo directament o amb la següent ordre:

```bash
# Localització fitxer
[marc@localhost kubernetes-project]$ ll ~/.kube/config 
-rw-------. 1 marc marc 450 may 19 18:24 /home/marc/.kube/config
# Ordre
[marc@localhost kubernetes-project]$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/marc/.minikube/ca.crt
    server: https://172.17.0.2:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/marc/.minikube/profiles/minikube/client.crt
    client-key: /home/marc/.minikube/profiles/minikube/client.key
```

Podem obtindre informació del clúster amb l'ordre `kubectl cluster-info`:

```bash
[marc@localhost kubernetes-project]$ kubectl cluster-info
Kubernetes master is running at https://172.17.0.2:8443
KubeDNS is running at https://172.17.0.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

# Instal·lació Minikube

Descarreguem el paquet i l'instal·lem com ens indica a la seva [docu](https://minikube.sigs.k8s.io/docs/start/):

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-latest.x86_64.rpm
sudo rpm -ivh minikube-latest.x86_64.rpm
```

Per a arrencar-lo juntament amb *Docker* ho indiquem amb el *start* tal com ens indica la seva [docu](https://minikube.sigs.k8s.io/docs/drivers/docker/):

```bash
# Executem el clúster fent server Docker
minikube start --driver=docker
# Si volguèssim executar Docker com a default
minikube config set driver docker
```

Just després d'executar-lo ens demanarà que instal·lem `kubectl`:

```bash
[marc@localhost ~]$ minikube start
😄  minikube v1.10.1 en Fedora 29
✨  Using the docker driver based on existing profile
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
💾  Downloading Kubernetes v1.18.2 preload ...
    > preloaded-images-k8s-v3-v1.18.2-docker-overlay2-amd64.tar.lz4: 525.43 MiB
🔥  Creating docker container (CPUs=2, Memory=2200MB) ...
🐳  Preparando Kubernetes v1.18.2 en Docker 19.03.2...
    ▪ kubeadm.pod-network-cidr=10.244.0.0/16
🔎  Verifying Kubernetes components...
🌟  Enabled addons: default-storageclass, storage-provisioner
🏄  Done! kubectl is now configured to use "minikube"
💡  Para disfrutar de un funcionamiento óptimo, instala kubectl: https://kubernetes.io/docs/tasks/tools/install-kubectl/
```

L'instal·lem tal com ens indica a la docu [Install and Set Up kubectl - Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)

```bash
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubectl
```

Un cop instal·lat i arrencat, podem comprobar el seu estat amb `status` tal com ens indica la seva [docu](https://kubernetes.io/docs/tasks/tools/install-minikube/):

```bash
# Mirem l'estat
[marc@localhost ~]$ minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
# Per a parar el clúster
minikube stop
```

# Accés al clúster

Podem accedir al clúster (ens val per a tots els clúster de kubernetes) de diferents formes:

* Mitjançant eines i scripts de CLI (*Command Line Interface*)

* Interfícies d'usuari mitjançant un navegador web

* Api's

## CLI

`kubectl` és el client CLI de Kubernetes per a administrar els recursos del clúster i les aplicacions. El podem fer servir tant com a ordre en la terminal o com a part de scripts d'eines d'automització. El podem fer servir remotament si els punts d'accesos i les credencials estàn configurades.

## Interfície web

`Kubernetes Dashboard` proveeix una interfície d'usuari basada en web per a interactuar amb un clúster de Kubernetes per a administar aquest i les aplicacions containeritzades.

Podem accedir-hi directament executant la següent ordre:

```bash
[marc@localhost kubernetes-project]$ minikube dashboard
🔌  Enabling dashboard ...
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:36391/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
```

També podem accedir-hi si executem primer l'ordre `kubectl proxy`. En aquest cas, si parem el proxy no hi podrem accedir.

![Kubernetes Dashboard](../aux/Kubernetes_dashboard.png)

## APIs

Com ja sabem, Kubernetes té un **API Server**, i ens hi podem connectar des de l'exterior per a interactuar amb el clúster. Podem connectar directament al servidor API amb els seus *endpoints* e introduïr-hi comandes, sempre que tinguem acces al node mestre i els permisos corresponents.

```bash
                                /

/healtz        /metrics        /api        /apis        ....

                  /api/v1                /apis/apps    /apis/...

   /api/v1/pods    /api/v1/nodes        /apis/apps/v1

                               /apis/apps/Deployment    /apis/apps/...
```

Podem tindre els següents espais HTTP:

* `Core Group`: Aquest grup inclou els objectes tals com Pods, Serveis, nodes, noms d'espai...

* `Named Group`: Aquest grup inclou els objectes en format /apis/$NAME/version... Aquestes versions poden incloure diferents nivells d'estabilitat (alpha, beta, stable)

* `System-wide`: Aquest grup consisteix en els *endpoints* de l'API, tal com */healthz*, */logs*, */metrics*...

Amb `kubectl proxy` executant-se, podem enviar requests al API server mitjançant *localhost* al port corresponent:

```bash
# Rebem els endpoints que té el server
curl http://localhost:8001/
{
 "paths": [
   "/api",
   "/api/v1",
   "/apis",
   "/apis/apps",
   ......
   ......
   "/logs",
   "/metrics",
   "/openapi/v2",
   "/version"
 ]
}
```

* En el cas de que no s'estigui executant `kubectl proxy` ens tindrem que autenticar directament al servidor API quan enviem les request.
  
  Podem fer-ho a través d'un *Bearer Token*, que és un token d'accés generat pel servidor autenticador (el servidor API al node mestre)  en el qual ens podem connectar de nou.
  
  ```bash
  # Conseguim el token
  TOKEN=$(kubectl describe secret -n kube-system $(kubectl get secrets -n kube-system | grep default | cut -f1 -d ' ') | grep -E '^token' | cut -f2 -d':' | tr -d '\t' | tr -d " ")
  # Conseguim l'endpoint
  APISERVER=$(kubectl config view | grep https | cut -f 2- -d ":" | tr -d " ")
  # Comprovem que tenim la mateixa ip mirant les següents ordres
  echo $APISERVER
  kubectl cluster-info
  # Finalment accedim
  curl $APISERVER --header "Authorization: Bearer $TOKEN" --insecure
  # També podem accedir-hi sense el token d'accés, extraient el certificat del client, la clau del client i el certificat d'autoritat del fitxer .kube/config
  # Un cop s'extreuen, són encodejats i passats amb la comanda curl per l'autenticació
  ```

# Creació d'un deployment

## Des del dashboard

Per a crear un nou deployment des del dashboard hem de seguir els següents pasos (tenim en compte de que tenim Minikube engegat i executant-se):

* Seleccionar el botó `+` de la part superior dreta.
  
  ![create new deployment 1](../aux/crea1.png)

* Ens apareixerà una interfície per crear-ho inserint el contingut d'un fitxer *YAML* o *JSON* , exportant el propi fitxer o creant-ho al propi dashboard. En aquest cas escollirem la tercera opció.
  
  ![crear deployment 2](../aux/crear2.png)

* Aquí podem escollir el nom que volguem, la imatge en la que es basaràn, el número de Pods... A *Show advanced options* podem especificar les etiquetes, el nom d'espai, les variables d'entorn... Per defecte, l'etiqueta *app* amb el valor *webserver* (el mateix nom que li donem) se li fica al nom de l'app.
  
  ![crear nou deployment dashboard](../aux/crea3.png)

* Un cop li donem al botó `Deploy` executem tota la creació. El `Deployment` **webserver** crearà el `ReplicaSet` **webserver-5d58b6b749** i aquest crearà els `Pods` especificats **webserver-5d58b6b749-X** (sent X el número del Pod). Tindrem una interfície on podrem veure tota la informació de cada element i el seu estat.
  
  ![crear nou deployment amb dashboard 4](../aux/crear4.png)

Podem comprovar que s'ha creat correctament si mirem amb `kubectl`:

```bash
[marc@localhost kubernetes-project]$ kubectl get deployments
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
webserver   3/3     3            3           6m42s
[marc@localhost kubernetes-project]$ kubectl get replicasets
NAME                   DESIRED   CURRENT   READY   AGE
webserver-5d58b6b749   3         3         3       6m55s
[marc@localhost kubernetes-project]$ kubectl get pods
NAME                         READY   STATUS    RESTARTS   AGE
webserver-5d58b6b749-gsl2d   1/1     Running   0          8m28s
webserver-5d58b6b749-k7r8g   1/1     Running   0          8m28s
webserver-5d58b6b749-z4d42   1/1     Running   0          8m28s
```

El podem eliminar amb l'opció `delete deployments nomDep`. Hem de tindre en compte de que si esborrem el *Deployment* també esborrarem els *ReplicaSets* i *Pods* que s'hagin creat per ell:

```bash
[marc@localhost kubernetes-project]$ kubectl delete deployments webserver
deployment.apps "webserver" deleted
[marc@localhost kubernetes-project]$ kubectl get replicasets
No resources found in default namespace.
[marc@localhost kubernetes-project]$ kubectl get pods
No resources found in default namespace.
```

## Des de CLI

Podem crear un nou deployment des de la línia de comandes a través d'un fitxer. En aquest exemple el crearem amb un fitxer *YAML*

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webserver
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
```

Per a fer-ho utilitzem la comanda `kubectl` junt amb l'opció `create` i l'argument `-f` per indicar-li el fitxer de configuració:

```bash
[marc@localhost config_files]$ kubectl create -f webserver.yaml 
deployment.apps/webserver created
```

Com podem comprovar, el *Deployment* ha creat el *Replicaset* i aquest els *Pods*:

```bash
[marc@localhost config_files]$ kubetl get deployments
NAME        READY   UP-TO-DATE   AVAILABLE   AGE
webserver   3/3     3            3           3m7s
[marc@localhost config_files]$ kubectl get replicasets
NAME                  DESIRED   CURRENT   READY   AGE
webserver-97499b967   3         3         3       3m13s
[marc@localhost config_files]$ kubectl get pods
NAME                        READY   STATUS    RESTARTS   AGE
webserver-97499b967-5s6ws   1/1     Running   0          3m17s
webserver-97499b967-5v448   1/1     Running   0          3m17s
webserver-97499b967-x7x52   1/1     Running   0          3m17s
```

Si ens connectem a la IP del clúster amb el port mapejat, ens connectarà amb l'nginx

```bash
[marc@localhost config_files]$ minikube ip
172.17.0.2
```

![nginx del clúster](../aux/nginx_cluster.png)

## Reptes de xarxa

Kubernetes, com un orquestrador de microserveis containeritzats encara 4 reptes de xarxa:

* `Comunicació entre contàiners dintre dels Pods`: El *container runtime* crea una xarxa isolada. A Linux, aquesta xarxa es refereix a un espai de nom. El mateix espai de nom es comparteix entre els contàiners, o amb el host del sistema operatiu.
  
  Quan s'executa un *Pod* es crea un espai de nom dins d'aquest i tots el contàiners que s'executen dins del *Pod* comparteixen aquesta xarxa, poguent parlar entre ells.

* `Comunicació entre els Pods al mateix node i altres nodes del clúster`: En un clúster de Kubernetes, els *Pods* es llençen de forma aleatoria entre els nodes *treballadors*. Independenment del seu node host, els *Pods* s'espera que es puguin comunicar entre ells sense implementar la NAT.
  
  El model de xarxa de Kubernetes pretén reduir aquesta complexitat i tracta els *Pods* en una xarxa. Cada un rep una adreça IP. Aquest model s'anomena **Ip-per-Pod** i assegura la comunicació entre *Pods*.
  
  En el cas dels contàiners, comparteixen el espai de nom del *Pod*  i necessiten coordinar l'assignament de ports dins del *Pod*, tot això a la vegada de comunicar-se entre ells. Tot i això els contàiners estàn integrats amb el model de networking de Kubernetes fent servir el **Container Network Interface (CNI)** que suporten els **CNI plugins**.
  
  **CNI** és un conjunt d'especificacions i llibreries que admet plugins per a configurar el networking dels contàiners. Hi han uns quants *core* plugins, però la majoria són plugins **Software Defined Networking (SDN)** de tercers que implementen el model de xarxa de Kubernetes.

* `Comuncació entre el Pod i el servei dins del mateix espai de noms i entre altres espai de noms del clúster`

* `Comunicació de l'exterior al servei per a l'accés dels clients cap a les aplicacions del clúster`: Kubernetes habilita l'accés extern a través de serveis, construccions complexes que encapsulen les regles de xarxa en els nodes del clúster. Exposant aquests serveis al món exterior mitjançant el **kube-proxy**, les aplicacions són accessibles des de fora del clúster amb una IP virtual.

## Kubernetes Object Model

Kubernetes té un model d'objecte molt ric, representant diferents entitats persistents en el clúster. Aquestes entitats descriuen:

* Quines aplicacions containeritzades estem executant i a quin node.

* Consum de les aplicacions.

* Diferents polítiques adjuntes a les aplicacions, com polítiques *restart/upgrade*, tolerància a caigudes...

Amb cada objecte declarem la nostre intenció a la secció `spec`. El sistema de Kuberentes administra la secció `status` per als objectes, on enregistra l'estat actual. En qualsevol moment donat, el *Kubernetes Control Panel* intenta fer coincidir l'estat actual de l'objecte amb l'estat dessitjat.

En crear un objecte, les dades de la configuració d'aquest s'especifiquen a sota de la secció `spec` i s'envien al API server. La secció `spec` descriu l'estat dessitjat, conjuntament amb informació bàsica tal com el nom de l'objecte. El API request per a la creació d'un objecte ha de tindre la secció `spec`, ademés d'altres detalls. Tot i que el servidor API accepeta les definicions dels objectes en fitxers tipus *JSON*, la majoria de cops li hi facilitarem en format *YAML*, el qual es convertit en *JSON* quan s'envia al servidor API per `kubectl`.

Un exemple en format *YAML*:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.15.11
        ports:
        - containerPort: 80
```

* El camp `apiVersion` és el primer camp requerit, especifica **l'endpoint** al servidor API al que volem connectar. Ha de fer match amb una versió existent per al tipus d'objecte definit.

* El segon camp requerit és `kind`, on s'especifica el tipus d'objecte (en l'exemple és *Deployment*, però pot ser *Pod, Replicaset,  Namespace, Service.*..)

* El tercer camp requerit és `metadata` que conté l'informació bàsica, tal com el nom, etiquetes (*labels*), namespace...

* El quart camp requerit és `spec`, que marca el principi del bloc definint l'estat dessitjat de l'objecte. En l'exemple, volem assegurar-nos de que tenim 3 Pods executant-se en qualsevol moment. Els Pods es creen fent servir la plantilla (Template) definida a `spec.template`. Un objecte nidificat, tal com un Pod que forma part de *Deployment*, reté la seva `metadata` i `spec` i perd la `apiVersion` i `kind`, els dos es substitueixen per `template`.

* A `spec.template.spec` definim l'estat dessitjat del Pod. El aquest cas, el nostre Pod crea un únic contàiner executant l'imatge *nginx:1.5.11* de *Docker Hub*.

### Pods

Un *Pod* és l'objecte de Kubernetes més petit i senzill. És la unitat de desplegament a Kubernetes, el qual representa una sola instància de l'aplicació. Un *Pod* és la col·lecció lògica d'un o més contàiners que:

* Estàn programats junts al mateix host amb el Pod.

* Comparteixen el mateix network namespace.

* Té accés per a montar el mateix emmagatzematge extern (volums).

Els *Pods* són de naturalesa efímera, i no tenen la capacitat d'auto curar-se a ells mateixos. És per això que són utilitzats juntament amb controladors que poden gestionar replicació de *Pods*, tolerància a caigudes, auto-curació... Exemples d'aquests controladors són *Deployments*, *ReplicaSets*, *ReplicationControllers*... 

Enllaçem l'especificació dels *Pods* nidificats a un controlador fent servir **Pod Template**.

Exemple de configuració d'un Pod en format *YAML*:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.15.11
    ports:
    - containerPort: 80
```

* El camp `apiVersion` ha d'especificar **v1** per a la definició de l'objecte del *Pod*.

* El segon camp requerit és `kind` que especifica que és un objecte tipus *Pod*.

* El tercer camp requerit és `metadata`, que conté el nom de l'objecte i l'etiqueta.

* El quart camp requerit és `spec`, que marca el principi del bloc on definim l'estat dessitjat de l'objecte *Pod* (també anomenat `PodSpec`)

* En aquest exemple el nostre *Pod* crea un sol contàiner executant l'imatge *nginx:1.5.11* de *Docker Hub*.

Podem obtindre informació del Pod amb l'opció `describe pod nomPod` de `kubectl`:

```bash
[marc@localhost kubernetes-project]$ kubectl describe pod webserver-5d58b6b749-z4d42
Name:         webserver-5d58b6b749-z4d42
Namespace:    default
Priority:     0
Node:         minikube/172.17.0.2
Start Time:   Tue, 16 Jun 2020 12:01:27 +0200
Labels:       k8s-app=webserver
              pod-template-hash=5d58b6b749
Annotations:  <none>
Status:       Running
IP:           172.18.0.8
IPs:
  IP:           172.18.0.8
Controlled By:  ReplicaSet/webserver-5d58b6b749
Containers:
  webserver:
    Container ID:   docker://34c4a37b61c6b3866a676779c63766b18b942cb33e637ed23809bc7dfbb5daad
    Image:          nginx:alpine
    Image ID:       docker-pullable://nginx@sha256:b89a6ccbda39576ad23fd079978c967cecc6b170db6e7ff8a769bf2259a71912
    Port:           <none>
    Host Port:      <none>
    State:          Running
      Started:      Tue, 16 Jun 2020 12:02:49 +0200
    Ready:          True
    Restart Count:  0
    Environment:    <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-llmhz (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  default-token-llmhz:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-llmhz
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:          <none>
```

També podem obtindre els pods d'una etiqueta en particular si li afegim l'argument `-l`:

```bash
[marc@localhost kubernetes-project]$ kubectl get pods -l k8s-app=webserver
NAME                         READY   STATUS    RESTARTS   AGE
webserver-5d58b6b749-gsl2d   1/1     Running   0          6h3m
webserver-5d58b6b749-k7r8g   1/1     Running   0          6h3m
webserver-5d58b6b749-z4d42   1/1     Running   0          6h3m
```

## Labels (etiquetes)

Les *labels* (etiquetes) són parells de clau-valor. S'utilitzen per organitzar i seleccionar un subconjunt d'objectes, basat en els requeriments del lloc. Ens podem trobar que varis objectes tinguin les mateixes etiqueta/es, amb el que s'extreu que les etiquetes no proveeïxen singularitat a un objecte.

Els controladors fan servir etiquetes per agrupar de forma lògica objectes desacoplats, en comptes de fer servir noms d'objectes o IDs.

Per exemple, tenim els següents objectes amb les seves etiquetes:

```bash
------------------                    ------------------     
|                |                    |                |
|  app:frontend  |                    |  app:backend   |
|  env:dev       |                    |  env:dev       |
|                |                    |                |
------------------                    ------------------

------------------                    ------------------
|                |                    |                |
| app:frontend   |                    | app:backend    |
| env:qa         |                    | env:qa         |
|                |                    |                |
------------------                    ------------------
```

En aquest exemple tenim dues claus: `app` i `env`. Segons els nostres requeriments, els hi hem donat diferents valors als nostres Pods.  L'etiqueta `env=dev` selecciona i agrupa els dos d'adalt, mentres que l'etiqueta `env=frontend` selecciona i agrupa els dos de l'esquerra. Podem seleccionar només un seleccionant les dues etiquetes: `app=frontend` i `env=qa`.

### Label Selectors

Els controladors fan servir els *Labels Selectors* per a seleccionar un subconjunt d'objectes. Kubernetes en suporta dos tipus:

* `Equality-Based Selectors (selectors basats en l'igualtat)`: Els selectors basats en l'igualtat permeten el filtratge dels objectes basat en les etiquetes de clau i valor. Per als matches es fan servir els simbols `= o ==` (igual, es poden fer servir els dos) o `!=` (diferents). 
  
  Per exemple, amb `env==dev` o `env=dev` estem seleccionant els objectes on l'etiqueta *env* tingui el valor *dev*.

* `Set-Based Selectors (selectors basats en un conjunt)`: Els selectors basats en un conjunt permeten el filtratge dels objectes basat en un conjunt de valors. Podem fer servir els operadors `in/ not in` per als valors de les etiquetes i els operadors `exist/ does not exist` per a les claus. 
  
  Per exemple, amb `env in (dev,qa)` estem seleccionant els objectes que la seva etiqueta *env* sigui **dev** o **qa**; amb `!app` seleccionem els objectes que la seva clau no sigui **app**.

```bash
# Amb env=dev seleccionem les dues de dalt
------------------                    ------------------     
|                |                    |                |
|  app:frontend  |                    |  app:backend   |
|  env:dev       |                    |  env:dev       |
|                |                    |                |
------------------                    ------------------

------------------                    ------------------
|                |                    |                |
| app:frontend   |                    | app:backend    |
| env:qa         |                    | env:qa         |
|                |                    |                |
------------------                    ------------------
```

## Replication Controllers (controladors de rèpliques)

Tot i que ja no és mètode recomenat, un `ReplicationController` és un controlador que s'assegura que un número especificat de rèpliques d'un Pod s'estàn executant en qualsevol moment. Si hi han més Pods dels dessitjats el controlador finalitzarà automàticament els Pods extra, al contrari, si hi han menys el controlador crearà més Pods per tindre els dessitjats. En general no despleguem un Pod independent, ni hauria de ser capaç de re-executar-se si finalitza amb un error. El mètode recomenat és fer servir algún tipus de controlador de rèpliques per crear i administrar els Pods.

El controlador per defecte és el `Deployment` el qual configura un `ReplicaSet` per administrar el cicle de vida dels Pods.

## ReplicaSets

El `ReplicaSet` és la següent generació del `ReplicationController`. Suporta tant els selectors basats en igualtat tant com els de conjunt, mentre que els *ReplicationControllers* sol suporten els basats en igualtat (actualment és la única diferència).

Amb el `ReplicaSet` podem escalar el número de Pods que executan una imatge d'una aplicació d'un contàiner. El escalat es pot fer de forma manual o fent servir un *autoscaler*.

En el cas de que algún Pod falli (per falta de recursos, obligat...) l'estat actual serà diferent del dessitjat (`current != desired`) i, de forma automàtica, el `ReplicaSet` crearà un Pod adicional, per assegurar-se de que l'estat actual coincideix amb el dessitjat.

Els `ReplicaSets` es poden fer servir com a controladors de Pods independents però sol ofereixen unes característiques limitades. Per a controladors d'orquestració de Pods es recomana els `Deployments`.

Els `Deployments` administren l'actualització,  creació i eliminació dels Pods. El `Deployment` crea automàticament un `ReplicaSet`, el qual crea el Pod, així no cal que administrem el `ReplicaSet` i els Pods per separat, el `Deployment` ho administrarà pero nosaltres. 

## Deployments

Els objectes `Deployment` proveeixen actualitzacions declarades als Pods i `ReplicaSets`. El `DeploymentController` forma part del **controller manager** del node mestre, i s'assegura que l'estat actual sigui sempre l'estat dessitjat. Permet actualitzacions i downgrades sense problemes a través de **rollouts** i **rollbacks**, i administra directament els `ReplicaSets` per a l'escalat de les aplicacions.

En el següent exemple tenim un `Deployment` que crea un `ReplicaSet A`, el qual crea 3 *Pods* (cada template executa una imatge de contàiner nginx:1.7.9). En aquest cas, el `ReplicaSet A` s'associa amb el nginx:1.7.9 representant l'estat del `Deployment`. Aquest estat particular es desa com **Revision 1**.

```bash
                           replicas: 3
                           template: pod
                          image: nginx:1.7.9
                                 |
                                 |                                  
                                 |
                                 |  ReplicaSet A
                    ----------------------------
                    |    replicas: 3           |
                    |    template: pod         |
                    |    image: nginx:1.7.9    |
                    |            |             |
                    |          / | \           |
                    |         /  |  \          |
                    |        /   |   \         |
                    |    Pod1  Pod2  Pod3      |
                    ----------------------------

                    Deployment (ReplicaSet A Created)
```

Ara, canviant el template dels Pods, actualitzem la imatge del contàiner de la versió de nginx 1.7.9 a la 1.9.1. El `Deployment` dispararà un nou `ReplicaSet B` per a la nova versió de l'imatge i aquesta nova associació representa un nou estat del `Deployment` anomenat **Revision 2**. Aquesta transició entre els dos ReplicaSets és un **rolling update**.

Un `rolling update` es dispara quan actualitzem els template dels Pods per al  desplegament. Les operacions com l'escalat o l'etiquetatge del desplegament no el disparen ja que no canvien l'estat.

Un cop s'hagi completat el **rolling update**, el `Deployment` mostrarà els dos ReplicaSets, on l'**A** es escalat a **0** Pods i el **B** és escalat a **3**. Així és com el `Deployment` enregistra l'estat anterior de la configuració, com a **Revisions**.

```bash
                           replicas: 3
                           template: pod
                          image: nginx:1.9.1
                              /        \
                  antic      /          \      nou                    
                            /            \
   ReplicaSet A            /              \             ReplicaSet B
   ----------------------------            ----------------------------
   |    replicas: 3           |            |    replicas: 3           |
   |    template: pod         |            |    template: pod         |
   |    image: nginx:1.7.9    |            |    image: nginx:1.9.1    |
   |            |             |            |            |             |
   |          / | \           |            |          / | \           |
   |         /  |  \          |            |         /  |  \          |
   |        /   |   \         |            |        /   |   \         |
   |    Pod1  Pod2  Pod3      |            |    Pod1  Pod2  Pod3      |
   ----------------------------            ----------------------------

                    Deployment (ReplicaSet B Created)
```

Un cop el nou ReplicaSet i els seus Pods estiguin preparats, el `Deployment` començarà a administrar-los. Tot i això, el Deployment segueix guardant la configuració del estats anteriors el qual té un factor clau en la capacitat **rollback** del Deployment (retornat a una configuració anterior desada a un estat). En aquest exemple, si la nova versió a la que hem actualitzat no fós satisfactoria, podem fer un **rollback** a un estat anterior del Deployment (de la **Revision 2**(versió 1.9.1) a la **Revision 1**(versió 1.7.9)).

## Namespaces (espais de nom)

Si varis usuaris/equips fan servir el mateix clúster Kubernetes podem particionar el clúster en sub-clústers virtuals fent servir `Namespaces`. Els noms dels recursos/objectes dins un **Namespace** son únics, però no entre els demés **Namespaces** del clúster.

Per a llistar tots el **Namespaces** executem la comanda `kubectl get namespace`

```bash
$ kubectl get namespaces
NAME              STATUS       AGE
default           Active       11h
kube-node-lease   Active       11h
kube-public       Active       11h
kube-system       Active       11h
```

Generalment, Kubernetes crea quatre Namespaces per defecte:

* `kube-system`: Conté els objectes creats pel sistema Kubernetes, la majoria són agents de control.

* `default`: Conté els recursos i objectes creats pels administradors i desenvolupadors. Per defecte, ens connectem a aquest Namespace.

* `kube-public`: És un Namespace especial, el qual no és segur i llegible per a tothom. Es fa servir per a propòsits especials com mostrar al públic informació (no sensitiva) del clúster.

* `kube-node-lease`: És el Namespace més nou, el qual conté el objectes lease del node utilitzats en les dades del cor del node.

## Autenticació

Kubernetes no té un objecte anomenat *user*, ni tampoc desa *usernames* o altres detalls en la seva base d'objectes. Tot i això, els pot fer servir per control d'accés i peticions de logging.

Tenim dos tipus d'usuaris:

* `Usuaris normals`: S'administren fora del clúster mitjançant serveis independents com certificats user/client, un fitxer amb la llista d'usernames/passwords, comptes Google...

* `Comptes de servei`: Amb aquest tipus d'usuaris, el processos interns del clúster es comuniquen amb l'API server per realitzar diferents operacions. La majoria d'aquests usuaris es creen automàticament, però també es poden crear manualment. Aquests usuaris estàn lligats a un Namespace i es monten amb les credencials corresponents per comunicar-se amb l'API server com a **Secrets**.

Si està ben configurat, Kubernetes també suporta **peticions anònimes**. També suporta la suplantació d'usuaris per ser capaç d'actuar com un altre usuari, una característica útil pels administradors quan hi han errors en les polítiques d'autorització.

Per a l'autenticació, Kubernetes fa servir diferents *mòduls d'autenticació*:

* `Certificats de client`: Per habilitar l'autenticació amb certificats, necessitem referenciar a un fitxer que contingui un o més certificats d'autoritats passant l'opció `--client-ca-file=FILE` a l'API server. Els certificats mencionats en el fitxer hauràn de validad els certificats del client presentat a l'API server.

* `Fitxer estàtic de Token`: Podem passar un fitxer que contingui tokens predefinits amb l'opció `--token-auth-file=FILE` a l'API server. Aquests tokens duraràn de forma indefinida, i no es poden canviar sense reiniciar el servidor API.

* `Tokens d'arrencada`: Aquesta característica es troba en fase beta i es fa servir majoritariament a l'arrencada d'un nou clúster.

* `Fitxer estàtic de passwords`: Similar al fitxer de Tokens, podem passar el fitxer que contingui informació bàsica de l'autenticació amb l'opció `--basic-auth-file=FILE`. Aquestes credencials duraràn de forma indefinida i no les contrassenyes no es podràn canviar sense reiniciar el servidor API.

* `Tokens de comptes de servei`: Aquest autenticador ja està habilitat per defecte i fa servir tokens signats per a verificar les peticions. Aquests tokens es lliguen als Pods fent servir el *ServiceAccount Admission Controller*, el qual admet que els processos interns del clúster es comuniquin amb el servidor API.

* `OpenID Connect Tokens`: OpenID Connect ens ajuda a connectar amb proveïdors *OAuth2*, tal com *Azure Active Directory*, *Salesforce*, *Google*... Per a l'autenticació amb serveis externs.

* `Webhook Token Authentication`: Amb l'autenticació basada en Webhook, la verificació dels tokens poden relegar a un servei remot.

* `Proxy d'autenticació`: Si volguèssim programar autenticació adicional, podem fer servir un proxy d'autenticació.

Es poden habilitar múltiples autenticadors, sent el primer mòdul que autentiqui les peticions amb èxit qui farà l'evaluació. Per a poguer funcionar correctament, s'hauria d'habilitar al menys dos mètodes: l'autenticador de tokens de comptes de servei i un d'autenticació d'usuari.

## Autorització

Després d'una autenticació, els usuaris poden enviar les peticions API per realitzar diferents operacions. Aquestes peticions s'autoritzen per Kubernetes mitjançant varis mòduls d'autorització.

Alguns dels atributs d'aquestes peticions (com l'user, el grup, Resource o Namespace...) són revistats per Kubernetes. A continuació, aquesta tributs són evaluats segons les polítiques pertinents. Si l'evaluació és satisfactoria, la petició s'acepta, si no, es denega.

De forma similar a l'autenticació, l'autorització té múltiples mòduls. Es poden configurar més d'un mòdul per un clúster, i cada mòdul es comprova en seqüència. Si qualsevol mòdul aprova o denega una petició, aquesta decissió es retorna inmediatament.

Tenim els següents mòduls:

* `Node Authorizer`: És un mode especial el qual especificament autoritza peticions API fetes per **Kubelets**. Autoritza les operacions de lectura de Kubelets per a serveis, endpoints, nodes... I escriu les operacions per nodes, pods, events...

* `Attribute-Based Access Control (ABAC) Authorizer`:  Amb l'autoritzador ABAC (Control d'Accés Basat en Atribut) Kubernetes garanteix l'accés per les peticions API, combinant polítiques amb els atributs. En l'exemple que tenim, l'usuari *student* sol pot llegir Pods que estiguin al Namespace *lfs158*:
  
  ```json
  {
    "apiVersion": "abac.authorization.kubernetes.io/v1beta1",
    "kind": "Policy",
    "spec": {
      "user": "student",
      "namespace": "lfs158",
      "resource": "pods",
      "readonly": true
    }
  }
  ```
  
  Per a habilitar-ho, necessitarem iniciar el servidor API amb l'opció `--authorization-mode=ABAC`. També necessitarem especificar la política d'autorització amb `--authorization-policy-file=PolicyFile.json`.

* `Webhook Authorizer`: Amb aquest mòdul, Kubernetes pot oferir autoritzacions a alguns serveis de tercers, el qual tornarà *true* per una autorització i *false* per denegar. Per a habilitar aquest mòdul hem d'iniciar el servidor API amb l'opció `--authorization-webhook-config-file=FILE`.

* `Role-Based Access Control (RBAC) Authorizer`: Amb aquest mòdul RBAC (Control d'Accés Basat en Rol) podem regular l'accés als recursos basat en els rols individuals dels usuaris. A Kubernetes podem tindre diferents rols que es poden adjuntar a subjectes com usuaris, comptes de servei... Mentres creem els rols, restringim l'accés als recursos per a operacions específiques, tals com *create*, *get*, *update*, *patch*... Aquestes operacions són referides com a *verbs* (?)
  
  A RBAC, podem crear dos tipus de rols:
  
  * `Role`: Amb **Role** garantim l'accés als recursos segons un **Namespace** específic.
  
  * `ClusterRole`:  Amb **ClusterRole** garantim els mateixos permisos que amb **Role**, però orientat a tot el clúster.
    
    ```yaml
    # Exemple amb Role
    kind: Role
    apiVersion: rbac.authorization.k8s.io/v1
    metadata:
      namespace: lfs158
      name: pod-reader
    rules:
    - apiGroups: [""] # "" indicates the core API group
      resources: ["pods"]
      verbs: ["get", "watch", "list"]
    ```
    
    Segons l'exemple, creem el rol *pod-reader*, el qual sol té accés a llegir els Pods que tinguin *lfs158* com a Namespace. Un cop el rol s'ha creat, podem vincular usuaris amb **RoleBinding**. En tenim dos tipus:
    
    * `RoleBinding`: Ens permet vincular usuaris al mateix namespace com un rol. També ens podem referir a un **ClusterRole**, el qual ens donarà permissos als recursos del **Namespace** definit al **Clusterrole **dins del **Namespace **del **RoleBinding**.
    
    * `ClusterRoleBinding`: Ens permet l'accés als recursos a nivell del clúster i a tots els **Namespaces**
      
      ```yaml
      # Exemple amb RoleBinding
      kind: RoleBinding
      apiVersion: rbac.authorization.k8s.io/v1
      metadata:
        name: pod-read-access
        namespace: lfs158
      subjects:
      - kind: User
        name: student
        apiGroup: rbac.authorization.k8s.io
      roleRef:
        kind: Role
        name: pod-reader
        apiGroup: rbac.authorization.k8s.io
      ```
      
      Com podem veure a l'exemple, garanteix l'accés de l'usuari *student* per a llegir els Pods amb el NameSpace *lfs158*
  
  Per a habilitar l'autoritzador RBAC necessitarem iniciar el servidor API amb l'opció `--authorization-mode=RBAC`. Amb el RBAC configurem les polítiques de forma dinàmica.

## Control d'Admissió

El Control d'Admissió es fa servir específicament per granular les polítiques de control d'accés, les quals inclouen permetre contàiners privilegiats, consultar la quota de recursos... Forcem aquestes polítiques fent servir diferents controladors d'admissió, com *ResourceQuota*, *DefaultStorageClass*, *AlwaysPullImages*... Sol tenen efecte quan les peticions API han sigut autenticades i autoritzades.

Per a fer servir controladors d'admissió, hem d'iniciar el servidor API amb l'opció `--enable-admission-plugins=NamespaceLifecycle,ResourceQuota,PodSecurityPolicy,DefaultStorageClass`, on podem separar els controladors per comes.

Kubernetes ja té alguns controladors activats per defecte, que podem veure quins són amb la següent ordre:

```bash
kube-apiserver -h | grep enable-admission-plugins
# A la versió 1.18 són:
NamespaceLifecycle, LimitRanger, ServiceAccount, TaintNodesByCondition, Priority, DefaultTolerationSeconds, DefaultStorageClass, StorageObjectInUseProtection, PersistentVolumeClaimResize, RuntimeClass, CertificateApproval, CertificateSigning, CertificateSubjectRestriction, DefaultIngressClass, MutatingAdmissionWebhook, ValidatingAdmissionWebhook, ResourceQuota
```

## Service

Per a accedir a una aplicació, l'usuari/client ha de connectar-se als Pods. Com els Pods són de naturalesa efímera, els recursos no poden tindre una IP estàtica, ja que els Pods poden finalitzar de cop o reprogramats basats en el requeriments existents.

Si ens fixem en el següent exemple entendrem millor a què ens enfrentem:

```bash
                app:frontend
              / 10.0.1.3
             /                   
            /
user/client --- app:frontend
                10.0.1.4
            \
             \
              \ app:db
                10.0.1.10
```

De sobte, el Pod al qual el client estava conectat finalitza, i el controlador crea un nou Pod. Aquest nou Pod té una altre adreça IP, la qual el client no la sap.

```bash
                app:frontend
              / 10.0.1.3
             /                   
            /
user/client ---  X|app:frontend|  app:frontend
                  |10.0.1.4    |  10.0.1.15
            \
             \
              \ app:db
                10.0.1.10
```

Per solucionar aquesta situació, Kubernetes proveeïx un objecte d'alt nivell anomenat `Service`, que agrupa de forma lògica els Pods i defineix una política d'accés a ells. Aquesta agrupació es fa amb `Labels` i `Selectors`.

Seguint amb l'exemple, `app` és l'etiqueta clau, sent *frontend* i *db* valors dels diferents Pods. Amb els selectors **app==frontend** i **app==db** ho agrupem en dos sets lògics; un amb 2 Pods i l'altre amb un únic Pod.

Assignem un nom al agrupament, anomenat `Service`. En l'exemple, creem dos *Services*, `frontend-svc` i `db-svc` amb els seus selectors (**app==frontend** i **app==db**) respectius.

```bash
               select app=frontend                  app:frontend
              / service = frontend-svc ------------ 10.0.1.3
             /  VIP = 172.17.0.4                                    
            /                     \               
user/client                        \---------------  app:frontend
            \                                          10.0.1.4  
             \                                   
              \select app=db                    
               service = db-svc ------------------ app:db
               VIP = 172.17.0.5                    10.0.1.10        
```

Els *Services* poden exposar **Pods**, **ReplicaSets**, **Deployments**, **DaemonSets** i **StatefulSets**.

### Exemple de Service

A continuació tenim l'exemple de la definició d'un objecte *Service*:

```yaml
kind: Service
apiVersion: v1
metadata:
  name: frontend-svc
spec:
  selector:
    app: frontend
  ports:
  - protocol: TCP
    port: 80
    targetPort: 5000
```

En aquest exemple estem creant el *Service* `frontend-svc` seleccionant tots els Pods que tenen el valor *frontend* a l'etiqueta clau *app*. Per defecte, cada *Service* rep una IP enrutable sol dins del clúster, coneguda com `ClusterIP`.

### Accés als Pods fent servir l'objecte Service

L'usuari/client ara es connecta al *Service* via la seva *ClusterIP*, la qual reenvia el tràfic a un dels Pods seleccionats. El *Service*, per defecte, proveeïx balança de càrrega mentres selecciona els Pods pel reenviament del tràfic.

Mentres que el *Service* reenvia el tràfic als Pods, podem seleccionar el `targetPort` al Pod que rep el tràfic. En l'exemple, el nostre servei *frontend-svc* rep les peticions al port **80** i les reenvia a un dels Pods associat al seu `targetPort 5000`. Si no definissim  *targetPort*, el tràfic es reenviaria als Pods al mateix port on el *Service* rep el tràfic.

A cada adreça dels Pods, junt amb el seu `targetPort`, s'hi fa referència com a `Service endpoint`. En l'exemple, el *frontend-svc* té **2 endpoints**: *10.0.1.3:5000* i *10.0.1.4:5000*. Els *endpoints* són administrats i creats automàticament pel *Service*, no per l'administrador del clúster.

## kube-proxy

Tots els nodes treballadors executen un daemon anomenat `kube-proxy`, el qual vigila el servidor API del node mestre per a addicions o eliminacions de *Services* i *endpoints*.

**kube-proxy** configura les regles d'**iptables** per capturar el tràfic de la seva *ClusterIP* i ho reenvia a cada un dels endpoints del *Service*. Si eliminem un *Service*, **kube-proxy** elimina les regles d'iptables corresponents a tots els nodes.

### Service Discovery

Com els *Services* són el mode primari de comunicació a Kubernetes, necessitem una forma de descobrir-los mentres s'executen. Kubernetes suporta dos mètodes per descobrir els *Services*:

* `Variables d'entorn (Environment Variables)`: En el mateix moment que un Pod s'engega en un node treballador, el daemon `kubelet` que s'està executant afegeix un conjunt de variables d'entorn al Pod per tots els *Services* **actius**.
  
  Per exemple, si tenim activat un *Service* anomenat **redis-master**, el qual exposa el port **6379**, i que la seva *ClusterIP* és **172.17.0.6**, podrem trobar en el nou Pod les següents variables d'entorn:
  
  ```bash
  REDIS_MASTER_SERVICE_HOST=172.17.0.6
  REDIS_MASTER_SERVICE_PORT=6379
  REDIS_MASTER_PORT=tcp://172.17.0.6:6379
  REDIS_MASTER_PORT_6379_TCP=tcp://172.17.0.6:6379
  REDIS_MASTER_PORT_6379_TCP_PROTO=tcp
  REDIS_MASTER_PORT_6379_TCP_PORT=6379
  REDIS_MASTER_PORT_6379_TCP_ADDR=172.17.0.6
  ```
  
  Tot i això hem d'anar amb compte amb aquesta sol·lució, ja que si creem un *Service* després d'engegar un Pod aqust no tindrà les variables d'entorn corresponents al nou *Service*.

* `DNS`: Kubernetes té un add-on per DNS, el qual crea un registre DNS per a cada *Service* amb el format ***my-svc.my-namespace.svc.cluster.local***. Els *Services* amb el mateix *Namespace* troben altres *Services* sol amb el seu nom.
  
  Si afegíssim el *Service* **redis-master** al *Namespace* **my-ns**, tots els Pods del mateix *Namespace* el podrien trobar pel seu nom. Els Pods d'altres *Namespaces* el podrien trobar afegint el *Namespace* respectiu, tal com **redis-master.my-ns**.
  
  Aquesta és la sol·lució més comuna i recomenada.

### Tipus de Service (ServiceType)

Mentres definim un *Service*, també podem escollir el seu àmbit d'accés:

* Sol es pot accedir dins del clúster.

* Accessible tant des de dins com des de fora.

* Mapeja a una entitat la qual resideix tant dins com fora del clúster.

L'àmbit d'accés es decideix amb `ServiceType`, el qual es pot configurar quan creem el *Service*.

### ServiceType: ClusterIP i NodePort

`ClusterIP` és el *ServiceType* per defecte. Un *Service* rep una IP virtual, coneguda com la seva *ClusterIP*. Aquesta IP virtual es fa servir per comunicar-ser amb el *Service* i és accessible sol dins del clúster.

Amb el `NodePort` *ServiceType*, afegint-lo a *ClusterIP*,  un port de rang alt, escollit dinàmicament del rang per defecte **30000-32767**, és mapejat al *Service* respectiu, per a tots el nodes treballadors.

Per exemple, si el *NodePort* mapejat és el **32233** per al servei **frontend-svc**, si connectem a qualsevol treballador al port **32233** el node ens redirigirà automàticament tot el tràfic a la *ClusterIP* assignada (172.17.0.4). Si preferim un port de rang alt específic, el podem assignar al *NodePort*.

El *ServiceType* *NodePort* és útil quan volem que el nostre *Service* sigui accessible des del món exterior.

A continuació veurem com crear un *NodePort* directament des d'un fitxer *YAML*:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: web-service
  labels:
    run: web-service
spec:
  type: NodePort
  ports:
  - port: 80
    protocol: TCP
  selector:
    app: nginx
```

I per a crear-lo fem servir l'ordre `kubectl` amb l'opció `create`:

```bash
[marc@localhost config_files]$ kubectl create -f webserver-svc.yaml 
service/web-service created
```

O també podem exposar el Deployment ja creat (més ràpid i directe):

```bash
[marc@localhost config_files]$ kubectl expose deployment webserver --name=web-service --type=NodePort
service/web-service created
```

 Podem llistar els *Services* que tenim amb `kubectl`:

```bash
[marc@localhost config_files]$ kubectl get services
NAME          TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes    ClusterIP   10.96.0.1       <none>        443/TCP        31d
web-service   NodePort    10.107.61.170   <none>        80:30036/TCP   6m42s
```

Com podem observar, el servei que hem creat se li ha assignat la *ClusterIP* **10.107.61.170** i ha mapejat els ports **80:30036**, això vol dir que tenim el port estàtic **30036** al node i que quan ens hi connectem serem redirigits al port **80**.

No és necessari crear el *Deployment* primer i el servei després, ja que el servei trobarà i es connectarà als *Pods* segons el selector indicat.

Podem obtindre més detalls del servei amb l'opció `describe`:

```bash
[marc@localhost config_files]$ kubectl describe service web-service
Name:                     web-service
Namespace:                default
Labels:                   run=web-service
Annotations:              <none>
Selector:                 app=nginx
Type:                     NodePort
IP:                       10.107.61.170
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  30036/TCP
Endpoints:                172.18.0.6:80,172.18.0.7:80,172.18.0.8:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

`web-service` fa servir **app=nginx** com a selector per agrupar els nostres Pods, els quals estàn llistats com a *endpoints*. Quan una petició arribi al *Service* aquesta serà enviada a un dels Pods llistats en aquesta secció.

### ServiceType: LoadBalancer

Amb el *ServiceType* `LoadBalancer`:

* Tant `NodePort` com `ClusterIP` es creen automàticament, i el balançejador de càrrega extern els enrutarà directament.

* El *Service* està exposat en un port estàtic a cada node treballador.

* El *Service* està exposat de forma externa fent servir les capes del balançejador de càrrega del proveïdor del núvol.

El *LoadBalancer* només funciona si les capes inferiors de la infraestructura suporten la creació automàtica de balençajadors de càrrega i tenen el suport respectiu a Kubernetes, com és el cas de *Google Cloud Platform* i *AWS*. Si aquesta característica  no està configurada, el camp **IP address** del *LoadBalancer* no es carregarà, i el *Service* funcionarà com si estigués configurat amb *NodePort*.

### ServiceType: ExternalIP

Un *Service* pot ser mapejat cap a una adreça `ExternalIP` si aquesta pot enrutar cap a un o més nodes treballadors. El tràfic que s'insereix al clúster amb l'*ExternalIP* (com a la IP destí) al port del *Service* s'enruta cap a un dels endpoints d'aquest. Aquest tipus de servei requereix un proveïdor de cloud extern tal com *Google Cloud Platform* o *AWS*.

Hem de tindre en compte que *ExternalIPs* no les administra Kubernetes. L'administrador del clúster ha de configurar l'enrutament que mapejarà l'adreça *ExternalIP* cap a un dels nodes.

### ServiceType: ExternalName

`ExternalName` és un *ServiceType* especial, el qual no té selectors ni defineix cap endpoint. Quan s'hi accedeix amb el clúster, retorna un registre `CNAME` d'un *Service* configurat externament.

L'ús primari d'aquest *ServiceType* és habilitar *Services* configurats externament com `my-database.example.com` per a les aplicacions del nostre clúster. Si el *Service* extern es troba al mateix *Namespace*, utilitzant només el nom **my-database** estarà disponible per a les altres aplicacions i *Services* del mateix *Namespace*.

## Liveness i Readiness Probes

Quan tenim aplicacions/servidors executant-se en els nostres Pods dels nodes del nostre clúster, pot passar que no responguin o que tinguin un delay al arrencar.

Això ens pot suposar un problema, per a això implementar `Liveness and Readiness Probes` permet a `kubelet` portar un control de la salut de les aplicacions que s'estàn executant als Pods i forçar el reinici del contàiner si no rep una resposta. 

Si definim els dos es recomana habilitar el temps suficient per a `Readiness` per permetre fallar pocs cops abans de que passi, i llavors comprovar-ho amb `Liveness`. Si s'interposen entre ells pot haver-hi el risc de que el contàiner mai aconsegueixi l'estat ideal.

### Liveness

Si tenim un contàiner al Pod executant-se, però l'aplicació no respon a les nostres peticions significa que el contàiner no el podem fer servir. En comptes de fer-li un restart manual, podem fer servir `Liveness Probe`. Liveness comprova la salut de l'aplicació, i si aquest falla, `kubelet` reinicia el contàiner afectat de forma automàtica.

Podem definir el `Liveness` per:

* Comanda Liveness

* Petició HTTP Liveness

* Prova TCP Liveness

#### Comanda Liveness

En el següent exemple mirem l'existencia del fitxer `/tmp/healthy`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  labels:
    test: liveness
  name: liveness-exec
spec:
  containers:
  - name: liveness
    image: k8s.gcr.io/busybox
    args:
    - /bin/sh
    - -c
    - touch /tmp/healthy; sleep 30; rm -rf /tmp/healthy; sleep 600
    livenessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5
      periodSeconds: 5
```

El que estem fent és crear el fitxer especificat, comprovant que hi és cada 5 segons amb `periodSeconds`. El paràmetre `initialDelaySeconds` demana a *kubelet* que esperi 5 segons abans de la primera prova. Quan comprovi que el fitxer no hi és (perque l'hem esborrat) retorna l'error i el reiniciaria el Pod.

#### Petició HTTP Liveness

En aquest, *kubelet* envia una petició HTTP GET a l'endpoint */healthz* al port 8080. Si retorna un failure, *kubelet* reiniciarà el contàiner afectat.

```yaml
livenessProbe:
      httpGet:
        path: /healthz
        port: 8080
        httpHeaders:
        - name: X-Custom-Header
          value: Awesome
      initialDelaySeconds: 3
      periodSeconds: 3
```

#### Prova TCP Liveness

Amb aquesta prova, *kubelet* intenta obrir un socket TCP cap al contàiner que està executant l'aplicació. Si ho aconsegueix, l'aplicació és considerada sana, si no *kubelet* ho marcarà com que no està sà i reiniciarà el contàiner afectat.

```yaml
livenessProbe:
      tcpSocket:
        port: 8080
      initialDelaySeconds: 15
      periodSeconds: 20
```

### Readiness Probes

A vegades, les aplicacions han de presentar segons quines condicions abans de poguer servir tràfic. Aquestes condicions s'asseguren de que el servei del que depèn està preparat, o sapiguer que un dataset ha d'estar carregat... En aquests casos, fem servir `Readiness Probes` i esperem que passi una condició en particular. Quan passa, només en aquell moment l'aplicació pot servir tràfic.

Un Pod amb contàiners que no reportin que estàn preparats (*ready status*) no rebràn tràfic dels *Services*.

```yaml
readinessProbe:
  exec:
    command:
    - cat
    - /tmp/healthy
  initialDelaySeconds: 5
  periodSeconds: 5
```

# Kubernetes Volume Management

En un clúster Kubernetes, els contàiners dels Pods poden tant generar dades com consumir-les. Mentres que les dades del propi contàiner s'espera que no surtin d'ell, altres dades necessiten estar fora per agregar-les i possiblement carregades per les eines d'anàlisi. Kubernetes fa servir `Volums` de molts tipus i altres formes d'emmagatzematge per a l'administració de les dades del contàiner. Els objectes que ens ajuden amb els volums són `PersistentVolume` i `PersistentVolumeClaim`.

Com ja sabem, els contàiners executant-se als Pods són de naturalesa efímera, el que vol dir que tota les dades guardades dins del contàiner s'eliminen si aquest finalitza per algún crash. Tot i que *kubelet* el reiniciarà, ho farà de forma neta, el que vol dir que no tindrà cap dada anterior.

Per a solucionar aquest problema, Kubernetes fa servir els **volums**. Un volum és bàsicament un directori extern als contàiners. Aquest directori, el contingut i l'accés venen determinats per el `Volume Type (tipus de volum)`.

```bash
    --------------------                   --------------------
    |    Contàiner     |                   |     Contàiner    |
    --------------------                   --------------------
                       |                   |
                       |                   |
                       |                   |
                      -----------------------
                      |       Volum         |
                      -----------------------
```

A Kubernetes, el volum es vincula a un Pod i es pot compartir amb tots els contàiners del Pod. Això permet que les dades es preservin encara que el contàiner es reinicii.

## Volum Types (tipus de volums)

El directori que s'ha montat dins del Pod es veu respaldat a baix nivell pel `Volum Type`.  El *Volum Type* decideix les propietats del directori, com la mida, el contingut, els modes d'accés per defecte... Alguns exemples són:

* `emptyDir`: Aquest volum es crea per al Pod en el mateix moment que aquest arrenca en el node treballador. La vida del volum va lligada al Pod, pel que si el Pod finalitza, el contingut d'aquest s'esborra per sempre.

* `hostPath`: Amb aquest volum, podem compartir un directori del host on ens trobem. Si el Pod finalitza, el contingut segueix estant disponible al host.

* `gcePersistentDisk`:  Amb aquest tipus de volum, podem montar un volum **Google Compute Engine (GCE) persistent disk** a un Pod.

* `awsElasticBlockStore`: Amb aquest, podem montar un `AWS EBS Volume` a un Pod.

* `azureDisk`: Podem montar un `Microsoft Azure Data Disk` a un Pod.

* `azureFile`: Podem montar un `Microsoft Azure File Volume` a un Pod.

* `cephfs`: Podem montar un volum *CephFS* existent a un Pod. Quan el Pod finalitza, el volum es desmonta i el contingut es preserva.

* `nfs`: Podem montar un volum *NFS* a un Pod.

* `iscsi`: Podem montar un volum *iSCSI* a un Pod.

* `secret`: Amb aquest tipus, podem passar informació sensible, tal com contrasenyes, als Pods.

* `configMap`: Amb els objectes *configMap* podem proveïr dades de configuració, o comandes shell i arguments al Pod.

* `persistentVolumeClaim`: Podem vincular un *PersistentVolume* a un Pod fent servir *persistentVolumeClaim*.

## PersistentVolumes

En un entorn típic, l'emmagatzematge l'administraven els administradors i els usuaris només rebien les instruccions de com accedir a les dades.

Amb el contàiners ens trobem amb algo similar però és un repte més gran degut a tots els tipus de volums que hem vist. Kubernetes resol aquest problema amb el subsistema dels **volums persistents (PV)**, el qual proveeix APIs per als usuaris i administradors per administrar i consumir les dades persistents. Per a administrar-ho fa servir el tipus de recurs API `PersistentVolume`, i per a consumir-ho, fa servir el `PersistentVolumeClaim`.

Un volum persistent és un emmagatzematge connectat per xarxa al clúster, el qual és provist per l'administrador.

Els volums persistents es poden proporcionar de forma dinàmica basat en el recurs `StorageClass`. L'*StorageClass* conté paràmetres i proveïdors pre-definits  per crear un volum persistent. Fent servir `PersistentVolumeClaims` l'usuari envia una petició per la creació d'un PV dinàmic, el qual es transfereix al recurs *StorageClass*.

Alguns dels tipus de volums que suporta l'administració del emmagatzematge fent servir volums persistents són:

* GCEPersistentDisk

* AWSElasticBlockStore

* AzureFile

* AzureDisk

* CephFS

* NFS

* iSCI

Un exemple de volum persistent amb el tipus **hostPath**:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: k8s.gcr.io/test-webserver
    name: test-container
    volumeMounts:
    # On ho montarem dintre del Pod

    - mountPath: /test-pd
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # localització al host

      path: /data
      # amb directory el directori HA d'existir i tindre els permisos
      # necessaris
      # si volem que el crei en cas de que no hi sigui hem de ficar
      # DirectoryOrCreate (el crearà amb permisos 0755)
      # També pot ser File/OrCreate, Socket, CharDevice, BlockDevice

      type: Directory
```

## PersistentVolumeClaims

**PersistentVolumeClaims (PVC)** és una petició d'emmagatzematge feta per l'usuari. L'usuari demana per un recurs de volum persistent basat en el tipus, mode d'accés i mida. 

Hi han tres tipus de mode d'accés:

* `ReadWriteOnce`: Read-write per un sol node.

* `ReadOnlyMany`: Read-only per varis nodes.

* `ReadWriteMany`: Read-write per varis nodes.

Un cop es troba un volum persistent adeqüat, es vincula al *PersistentVolumeClaim*. Un cop s'ha vinculat correctament, es pot fer servir en un Pod.

Quan l'usuari finalitza el seu treball, el volum persistent es pot **alliberar**. Es pot **reclamar** (per un administrador per a verificar i/o afegir dades), **esborrar** (tant les dades com el volum s'esborren) o **reciclar per un ús futur** (només s'esborren les dades).

Un exemple de configuració de PVC (el nom del PVC ha de ser un nom de subdomini DNS vàlid):

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
```

I, per cridar-lo des d'un Pod:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```

## Container Storage Interface (CSI)

Sovint els orquestrador com Kubernetes, Mesos, Docker o Cloud Foundry solien tindre els seu mètodes propis per administrar l'emmagatzematge extern fent servir volums. Els venedors d'emmagatzematge i les comunitat de diferents orquestradors van començar a treballar junts per a estandaritzar l'interfície dels volums; un plugin per volums construit amb l'estandard CSI dissenyat per treballar a diferents orquestradors.

Actualment amb CSI, els proveïdors d'emmagatzematge poden desenvolupar solucions sense la necessitat de tocar el codi base de Kubernetes.

## ConfigMaps i Secrets

Mentres despleguem una aplicació, potser necessitem passar paràmentres com detalls de configuració, permisos, contrasenyes, tokens... Per exemple, pensem el cas de que necessitem desplegar deu aplicacions diferents per als nostres clients i, que per cada un, necessitem mostrar el nom de la companyia a l'UI. En comptes de crear 10 imatges diferents (una per client), podem fer servir la mateixa plantilla i passar-li el nom del client com un paràmetre. En aquests casos, podem fer servir el recurs `ConfigMap`. De forma similar, si el que volem passar és informarció sensible, podem fer servir el recurs `Secret`.

### ConfigMaps

`ConfigMaps` ens permet desacoplar els detalls de la configuració de l'imatge del contàiner. Fent-lo servir, passem dades de configuració com parelles de clau-valor, el qual és consumit pels Pods o qualsevol altre component del sistema i controladors, en forma de variables d'entorn, conjunt de comandes i arguments, o volums. Podem crear *ConfigMaps* des de valors literals, fitxers de configuració, d'es d'un o més directoris/fitxers...

El podem crear amb la comanda `kubectl create` i obtindre els seus detalls amb `kubectl get`:

```bash
# Creem el configmap
kubectl create configmap my-config --from-literal=key1=value1 --from-literal=key2=value2
configmap/my-config created 
# Mostrem els detalls de my-config
# amb -o yaml estem demanant que el resultat se'ns torni amb format yaml
$ kubectl get configmaps my-config -o yaml
apiVersion: v1
data:
  key1: value1
  key2: value2
kind: ConfigMap
metadata:
  creationTimestamp: 2019-05-31T07:21:55Z
  name: my-config
  namespace: default
  resourceVersion: "241345"
  selfLink: /api/v1/namespaces/default/configmaps/my-config
  uid: d35f0a3d-45d1-11e7-9e62-080027a46057
```

#### Crear un ConfigMap des d'un fitxer de configuració

Creem el fitxer de configuració amb el següent contingut:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: customer1
data:
  TEXT1: Customer1_Company
  TEXT2: Welcomes You
  COMPANY: Customer1 Company Technology Pct. Ltd.
```

Si al fitxer l'anomenem `nom-configmap.yaml` podem crear-lo directament amb:

```bash
kubectl create -f customer1-configmap.yaml
configmap/customer1 created
```

#### Utilitzar ConfigMaps dintre dels Pods

##### Com variables d'entorn

Dintre d'un contàiner, podem recuperar les dades clau-valor d'un *ConfigMap* o els valors específics de les claus de *ConfigMap* com a variables d'entorn.

En el següent exemple les variables d'entorn del contàiner **myapp-full-container** reben els valors de les claus del *ConfigMap* **full-config-map**:

```yaml
...
  containers:
  - name: myapp-full-container
    image: myapp
    envFrom:
    - configMapRef:
      name: full-config-map
...
```

En aquest altre exemple les variables d'entorn del contàiner `myapp-specific-container` reben els valors de les parelles clau-valor específiques de *ConfigMaps*:

```yaml
...
  containers:
  - name: myapp-specific-container
    image: myapp
    env:
    - name: SPECIFIC_ENV_VAR1
      valueFrom:
        configMapKeyRef:
          name: config-map-1
          key: SPECIFIC_DATA
    - name: SPECIFIC_ENV_VAR2
      valueFrom:
        configMapKeyRef:
          name: config-map-2
          key: SPECIFIC_INFO
...
```

Amb això conseguim les variables d'entorn específiques `SPECIFIC_ENV_VAR1` té el valor `SPECIFIC_DATA` des del *ConfigMap* **config-map-1**, i `SPECIFIC_ENV_VAR2` té el valor `SPECIFIC_INFO` des del *ConfigMap* **config-map-2**.

##### Com a volums

Podem montar un *ConfigMap* **vol-config-map** com a volum dintre del Pod. Per cada clau a *ConfigMap* es crea un fitxer al path on el montem (el fitxer rep el nom del nom de la clau) i el contingut del fitxer és el valor respectiu de la clau:

```yaml
...
  containers:
  - name: myapp-vol-container
    image: myapp
    volumeMounts:
    - name: config-volume
      mountPath: /etc/config
  volumes:
  - name: config-volume
    configMap:
      name: vol-config-map
...
```

### Secrets

Fiquem per exemple que tenim una aplicació Wordpress on el nostre frontend *wordpress* connecta amb una base de dades *MySQL* al backend fent servir una contrasenya. Mentres estem creant el *Deployment*  podem incloure la contrasenya del MySQL en el fitxer YAML, però no estarà protegida i estarà disponible per a qualsevol que tingui accés al fitxer.

Amb aquest escenari, l'objecte `Secret` ens pot ajudar encodejant l'informació sensible abans de compartir-la. Amb `Secrets` podem compartir informació sensible com contrasenyes, tokens, claus en forma de parelles clau-valor; a tot això se l'hi afegeix que podem controlar com es fa servir l'informació a *Secret*, reduïnt el risc d'exposar-lo sense voler. Als *Deployments* o altres recursos, l'objecte *Secret* és referenciat sense exposar el seu contingut. 

És important recordar que les dades de *Secret* s'emmagatzemen com a text plà dins de **etcd**, pel que els administradors han de limitar l'accés a l'API server i a **etcd**. Tot i això una nova característica permet que les dades de *Secret* s'encriptin mentres es guarden a **etcd**, aquesta característica s'ha d'habilitar al servidor API.

#### Creació d'un secret desde literal i comprovació dels seus detalls

Per a crear un *Secret*, podem fer servir la comanda `kubectl create secret`:

```bash
kubectl create secret generic my-password --from-literal=password=mysqlpassword
```

Amb aquesta comanda crearem un secret anomenat **my-password**, el qual té el valor **mysqlpassword** a la clau **password**.

Podem analitzar el secret amb els arguments `get` i `describe`. Cap d'aquestes mostraràn el contingut del secret. El tipus és llistat com `Opaque`:

```bash
$ kubectl get secret my-password
NAME          TYPE     DATA   AGE 
my-password   Opaque   1      8m

$ kubectl describe secret my-password
Name:          my-password
Namespace:     default
Labels:        <none>
Annotations:   <none>

Type  Opaque

Data
====
password:  13 bytes
```

#### Creació d'un secret de forma manual

Podem crear un secret de forma manual des d'un fitxer de configuració *YAML*. Farem servir el nom **mypass.yaml**. Hi han dos tipus de mapes per dades sensibles dintre de *Secret*: `data` i `stringData`.

Amb `data` cada valor del camp d'informació sensible ha d'estar encodejat amb **base64**. Si volem tindre un fitxer de configuració per al nostre secret, primer necessitem crear la nostra password encodejada amb **base64**:

```bash
$ echo mysqlpassword | base64
bXlzcWxwYXNzd29yZAo=
```

I fer-la servir al fitxer de configuració:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-password
type: Opaque
data:
  password: bXlzcWxwYXNzd29yZAo=
```

Hem de tindre en compte de que encodejar-la no vol dir encriptar-la, i qualsevol pot decodejar fàcilment les dades. És per això que ens hem d'assegurar de que no fem un commit del fitxer de configuració del secret al nostre còdi font.

Amb `stringData` no necessitem encodejar cada valor de l'informació sensible. El valor  del camp sensible s'encodeja quan el secret **my-password** es crea:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-password
type: Opaque
stringData:
  password: mysqlpassword
```

Amb el fitxer de configuració **mypass.yaml** podem crear el nostre secret amb la comanda `kubectl create`:

```bash
kubectl create -f mypass.yaml
secret/my-password created
```

#### Creació d'un secret des d'un fitxer

També podem crear un secret des del contingut d'un fitxer normal

Primer, hem de codejar l'informació sensible:

```bash
$ echo mysqlpassword | base64
 bXlzcWxwYXNzd29yZAo=

$ echo -n 'bXlzcWxwYXNzd29yZAo=' > password.txt
```

Ara creem el secret des del fitxer **password.txt**:

```bash
$ kubectl create secret generic my-file-password --from-file=password.txt
  secret/my-file-password created
```

Podem analitzar el secret amb els arguments `get` i `describe`. Cap d'aquestes mostraràn el contingut del secret. El tipus és llistat com `Opaque`:

```bash
$ kubectl get secret my-file-password
NAME               TYPE     DATA   AGE 
my-file-password   Opaque   1      8m

$ kubectl describe secret my-file-password
Name:          my-file-password
Namespace:     default
Labels:        <none>
Annotations:   <none>

Type  Opaque

Data
====
password.txt:  13 bytes
```

#### Utilitzar Secrets dintre dels Pods

Podem fer servir els secrets tant com variables d'entorn com volums.

##### Com variables d'entorn

En aquest cas referenciem sol la clau **password** del secret **my-password** i li assignem el seu valor a la variable d'entorn **WORDPRESS_DB_PASSWORD**:

```yaml
....
spec:
  containers:
  - image: wordpress:4.7.3-apache
    name: wordpress
    env:
    - name: WORDPRESS_DB_PASSWORD
      valueFrom:
        secretKeyRef:
          name: my-password
          key: password
....
```

##### Com un fitxer dintre del Pod

També podem montar un secret com un volum dintre del Pod. El exemple següent crea un fitxer per cada clau del secret **my-password** (on els fitxers s'anomenen després dels noms de les claus), els fitxers contenen els valors del *Secret*:

```yaml
....
spec:
  containers:
  - image: wordpress:4.7.3-apache
    name: wordpress
    volumeMounts:
    - name: secret-volume
      mountPath: "/etc/secret-data"
      readOnly: true
  volumes:
  - name: secret-volume
    secret:
      secretName: my-password
....
```

## Ingress

Amb els serveis, tenim regles de routing per cada un d'ells. Existeixen mentres el servei existeixi, i hi han moltes perquè hi han molts serveis en un clúster. Si poguèssim d'alguna forma desacoplar les regles de routing de l'aplicació i centralitzar l'administració d'aquestes regles, podriem actualitzar la nostra aplicació sense preocupar-nos per l'accés extern. Això ho podem fer amb el recurs `Ingress`.

Per habilitar les connexions entrants per accedir als *Services* del clúster, *Ingress* configura un balançejador de càrrega per al serveis a la capa 7 HTTP/HTTPS i proveeix el següent:

* TLS (Capa de Transport Segura)

* Hosting virtual basat en el nom

* Fanout Routing

* Balançejador de càrrega

* Regles customitzades

Amb *Ingress* els usuaris no connecten directament amb el servei. Primer arriben al *Ingress* endpoint, i a partir d'aquí, la petició es reenviada al servei dessitjat. Per exemple, tenim la següent configuració:

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: virtual-host-ingress
  namespace: default
spec:
  rules:
  - host: blue.example.com
    http:
      paths:
      - backend:
          serviceName: webserver-blue-svc
          servicePort: 80
  - host: green.example.com
    http:
      paths:
      - backend:
          serviceName: webserver-green-svc
          servicePort: 80
```

Com podem observar, l'usuari demana els dos webservers (blue i green) i anirà al mateix *Ingress* endpoint, i a partir d'aquí es reenviarà cap als serveis respectius. Aquest és un exemple de una regla d'ingrés al hosting virtual basat en el nom.

També podem tindre regles d'ingrés Fanout, quan les peticions a *example.com/blue* i *example.com/green* seràn redirigides cap als serveis **webserver-blue-svc** i **webserver-green-svc** respectivament:

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: fan-out-ingress
  namespace: default
spec:
  rules:
  - host: example.com
    http:
      paths:
      - path: /blue
        backend:
          serviceName: webserver-blue-svc
          servicePort: 80
      - path: /green
        backend:
          serviceName: webserver-green-svc
          servicePort: 80
```

El recurs *Ingress* no reenvia cap petició per si sol, només accepta les definicions de les regles enrutades del tràfic. L'ingress es completa amb l'`Ingress Controller`.

### Ingress Controller

L'`Ingress Controller` és una aplicació que vigila el servidor API del node mestre per canvis en els recursos d'ingrés i actualitza la capa 7 del balançejador de càrrega respectivament. Kubernetes suporta diferents controladors d'ingress, i si fos necessari, també el podem crear nosaltres. Els controlador que es fan servir de forma comú són **GCE L7 Load Balancer Controller** i **Nginx Ingress Controller**. Altres controladors són **Istio**, **Kong**, **Traefik**...

#### Engegar l'Ingress Controller amb Minikube

Minikube es llença amb el controlador de Nginx com un addon, deshabilitat per defecte. Es pot habilitar de forma senzilla amb la comanda següent:

```bash
minikube addons enable ingress
```

#### Desplegament del recurs Ingress

Un cop el controlador Ingress s'ha desplegat, podem crear un recurs Ingress fent servir `kubectl create`. Per exemple, si volem crear un fitxer **virtual-host-ingress.yaml** amb la regla de hosting virtual basat en el nom, farem servir la comanda següent:

```bash
kubectl create -f virtual-host-ingress.yaml
```

#### Accés als serveis fent servir Ingress

Amb el recurs Ingress creat, hauriem de ser capaços d'accedir als serveis d'abans accedint amb **blue.example.com**... Per a això hem d'actualitzar l'*/etc/hosts* amb la ip que ens dona Minikube.

## Anotacions (Annotations)

Amb les anotacions podem indicar dades no identificatives a qualsevol objecte amb el format clau-valor.

Al contrari que les etiquetes, les anotacions no es fan servir per identificar i seleccionar els objectes, es poden fer servir per a:

* Desar la build/release ID, números PR, git branch...

* Els directories de les entrades especificant quin tipus d'informació es pot trobar

* Punters cap a logging, monitoring, analítiques, eines per debug...

Per exemple, mentres creem un deployment, podem afegir una descripció:

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: webserver
  annotations:
    description: Deployment based PoC dates 2nd May'2019
....
```

I les anotacions es mostren quan describim l'objecte:

```bash
$ kubectl describe deployment webserver
Name:                webserver
Namespace:           default
CreationTimestamp:   Fri, 03 May 2019 05:10:38 +0530
Labels:              app=webserver
Annotations:         deployment.kubernetes.io/revision=1
                     description=Deployment based PoC dates 2nd May'2019
...
```

# Pràctiques

## Prova desplegament simple i rollout

A aquest prova desplegarem un nginx bàsic amb 2 pods, l'escalarem a 3 pods, actualitzarem la versió del nginx i tornarem a la versió anterior

### Desplegament

Despleguem amb la interfície web un deployment anomenat **nginx-basic** amb 2 Pods amb l'imatge **nginx:1.15-alpine**

O podem crear-ho amb un fitxer de configuració com aquest, anomenat `nginx_basic.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-basic
  labels:
    app: nginx-basic
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-basic
  template:
    metadata:
      labels:
        app: nginx-basic
    spec:
      containers:
      - name: nginx
        image: nginx:1.15-alpine
        ports:
        - containerPort: 80
```

### Escalat a 3 pods

```bash
[marc@localhost kubernetes-project]$ kubectl get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-basic-66d8b85c8f-27pj6   1/1     Running   0          2m4s
nginx-basic-66d8b85c8f-q6r5l   1/1     Running   0          2m4s
#
[marc@localhost kubernetes-project]$ kubectl scale deploy nginx-basic --replicas=3
deployment.apps/nginx-basic scaled
#
[marc@localhost kubernetes-project]$ kubectl get deploy,rs,po -l k8s-app=nginx-basic
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-basic   3/3     3            3           4m48s

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-basic-66d8b85c8f   3         3         3       4m48s

NAME                               READY   STATUS    RESTARTS   AGE
pod/nginx-basic-66d8b85c8f-27pj6   1/1     Running   0          4m48s
pod/nginx-basic-66d8b85c8f-q6r5l   1/1     Running   0          4m48s
pod/nginx-basic-66d8b85c8f-rjwr5   1/1     Running   0          88s
```

### Actualització de la versió i comprovació estats

Primer comprovem que sol tenim l'estat inicial, associat a l'imatge 1.15

```bash
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic# E
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
1         <none>
#
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=1
deployment.apps/nginx-basic with revision #1
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=66d8b85c8f
  Containers:
   nginx-basic:
    Image:    nginx:1.15-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
# Actualitzem la imatge
[marc@localhost kubernetes-project]$ kubectl set image deployment nginx-basic nginx-basic=nginx:1.16-alpine
deployment.apps/nginx-basic image updated
# Si mirem els estats ens trobem amb 2
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
1         <none>
2         <none>
# Si veiem l'informació del nou estat, veiem que té la nova imatge
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=2
deployment.apps/nginx-basic with revision #2
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=85c8bdb7c8
  Containers:
   nginx-basic:
    Image:    nginx:1.16-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
# Si mirem l'informació del deployment ens trobem que té 2 replicasets, un per cada estat
# I que aquest té 0 Pods, mentres que el nou té els 3
[marc@localhost kubernetes-project]$ kubectl get deploy,rs,po -l k8s-app=nginx-basic
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-basic   3/3     3            3           23m

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-basic-66d8b85c8f   0         0         0       23m
replicaset.apps/nginx-basic-85c8bdb7c8   3         3         3       5m7s

NAME                               READY   STATUS    RESTARTS   AGE
pod/nginx-basic-85c8bdb7c8-bx2q9   1/1     Running   0          3m35s
pod/nginx-basic-85c8bdb7c8-clnzj   1/1     Running   0          3m34s
pod/nginx-basic-85c8bdb7c8-nrv5d   1/1     Running   0          5m7s
```

### Rollback a un estat anterior

Si volguèssim, pel que fos, tornar a una versió anterior, hem de sapiguer a quina i quin és el seu estat

```bash
[marc@localhost kubernetes-project]$ kubectl rollout undo deployment nginx-basic --to-revision=1
deployment.apps/nginx-basic rolled back
# I si mirem el history ens trobem amb això
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
2         <none>
3         <none>
# L'estat 1 ara és la 3, sent l'estat actual
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=2
deployment.apps/nginx-basic with revision #2
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=85c8bdb7c8
  Containers:
   nginx-basic:
    Image:    nginx:1.16-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=5
deployment.apps/nginx-basic with revision #5
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=66d8b85c8f
  Containers:
   nginx-basic:
    Image:    nginx:1.15-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
# I com podem veure, el replicaset més antic és qui té els Pods actius
[marc@localhost kubernetes-project]$ kubectl get deploy,rs,po -l k8s-app=nginx-basic
NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nginx-basic   3/3     3            3           37m

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/nginx-basic-66d8b85c8f   3         3         3       37m
replicaset.apps/nginx-basic-85c8bdb7c8   0         0         0       18m

NAME                               READY   STATUS    RESTARTS   AGE
pod/nginx-basic-66d8b85c8f-k7st7   1/1     Running   0          4m8s
pod/nginx-basic-66d8b85c8f-kp8n2   1/1     Running   0          4m9s
pod/nginx-basic-66d8b85c8f-z4s52   1/1     Running   0          4m7s
```

### Quants estats desa?

Podem fer la prova afegint una nova versió de l'imatge:

```bash
[marc@localhost kubernetes-project]$ kubectl set image deployment nginx-basic nginx-basic=nginx:1.17-alpine
deployment.apps/nginx-basic image updated
# I comprovem que tenim un nou estat associat a la nova versió
[marc@localhost kubernetes-project]$ kubectl rollout history deployment nginx-basic
deployment.apps/nginx-basic 
REVISION  CHANGE-CAUSE
4         <none>
5         <none>
6         <none>
#
[marc@localhost kubernetes-project]$ kubectl rollout history deploy nginx-basic --revision=6
deployment.apps/nginx-basic with revision #6
Pod Template:
  Labels:    k8s-app=nginx-basic
    pod-template-hash=6898fd4dcf
  Containers:
   nginx-basic:
    Image:    nginx:1.17-alpine
    Port:    <none>
    Host Port:    <none>
    Environment:    <none>
    Mounts:    <none>
  Volumes:    <none>
```

Per tant per cada nova versió de l'imatge afegeix un nou estat, però si tornem a un estat **anterior**, tot i que canvia el número, segueixen havent-hi els **mateixos** estats.

## Prova de volums

En aquest pràctica crearem un volum local compartit entre dos Pods, hi escriurem alguna cosa, i comprovarem com, després d'eliminar aquests Pods, les dades segueixen estant (és a dir, són persistents)

### Creació volum local

Primer de tot, entrarem a Minikube mitjançant ssh i, un cop dins, crearem un directori.

```bash
[marc@localhost ~]$ minikube ssh
docker@minikube:~$ mkdir pod-volume
docker@minikube:~$ ll
total 44
drwxr-xr-x. 1 docker docker 4096 Jun 17 21:41 ./
drwxr-xr-x. 1 root   root   4096 Apr 27 17:43 ../
-rw-r--r--. 1 docker docker  220 Apr 27 17:43 .bash_logout
-rw-r--r--. 1 docker docker 3771 Apr 27 17:43 .bashrc
-rw-r--r--. 1 docker docker  807 Apr 27 17:43 .profile
drwxr-xr-x. 1 docker docker 4096 May 16 16:49 .ssh/
-rw-r--r--. 1 docker docker    0 May 16 16:49 .sudo_as_admin_successful
drwxr-xr-x. 2 docker docker 4096 Jun 17 21:41 pod-volume/
# Hem de sapiguer a on apuntaràn els nostres Pods
docker@minikube:~/pod-volume$ pwd
/home/docker/pod-volume
```

### Creació dels Pods que el faràn servir

Per a fer servir aquest volum ho hem d'indicar en el fitxer de configuració dels Pods que anomenarem `share-pod.yaml`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: share-pod
  labels:
    app: share-pod
spec:
  volumes:
  - name: host-volume
    hostPath:
      path: /home/docker/pod-volume
  containers:
  - image: debian
    name: debian
    volumeMounts:
    - mountPath: /host-vol
      name: host-volume
    command: ["/bin/sh", "-c", "echo aquest es el debian > /host-vol/dades.txt; sleep 3600"]
  - image: fedora
    name: fedora
    volumeMounts:
    - mountPath: /host-vol
      name: host-volume
    command: ["/bin/sh", "-c", "echo aquest es el fedora >> /host-vol/dades.txt; sleep 3600"]
```

Com podem observar, el que estem creant són dos contàiners dins d'un Pod (un debian i un fedora)  que montan el volum i escriuen en un fitxer.

Creem el Pod:

```bash
[marc@localhost config_files]$ kubectl create -f share-pod.yaml 
pod/share-pod created
# Comprovem que el Pod s'hagi creat i estigui corrent
[marc@localhost config_files]$ kubectl get pods
NAME                           READY   STATUS              RESTARTS   AGE
share-pod                      0/2     ContainerCreating   0          9s
[marc@localhost config_files]$ kubectl get pods
NAME                           READY   STATUS    RESTARTS   AGE
share-pod                      2/2     Running   0          19s
```

### Comprovació dades locals

Ara mirarem si, s'ha creat el fitxer i cada contàiner ha executat la seva ordre, desant així el contingut en el local (del node mestre en aquest cas per ser Minikube)

```bash
# Hem de tindre en compte que estem a /home/docker/pod-volume
docker@minikube:~/pod-volume$ ll
total 20
drwxr-xr-x. 2 docker docker 4096 Jun 17 22:27 ./
drwxr-xr-x. 1 docker docker 4096 Jun 17 21:41 ../
-rw-r--r--. 1 root   root     40 Jun 17 22:47 dades.txt
docker@minikube:~/pod-volume$ cat dades.txt 
aquest es el debian
aquest es el fedora
```

Com podem comprovar, s'ha creat correctament

### Persistencia de les dades: esborrar el Pod

Com ja sabem, els Pods son de naturalesa efímera (i per tant, les seves dades també) ara comprovarem com, esborrant el Pod, les dades segueixen estant en local.

```bash
[marc@localhost ~]$ kubectl delete pod share-pod
pod "share-pod" deleted
[marc@localhost ~]$ kubectl get pods
NAME                           READY   STATUS    RESTARTS   AGE
nginx-basic-6898fd4dcf-6lm92   1/1     Running   2          137m
nginx-basic-6898fd4dcf-qxjkt   1/1     Running   2          136m
nginx-basic-6898fd4dcf-xmkqq   1/1     Running   2          136m
webserver-97499b967-5s6ws      1/1     Running   3          30h
webserver-97499b967-5v448      1/1     Running   3          30h
webserver-97499b967-x7x52      1/1     Running   3          30h
```

Tornem a comprovar que el fitxer està en local i que, evidentment, les dades també:

```bash
docker@minikube:~/pod-volume$ ll
total 20
drwxr-xr-x. 2 docker docker 4096 Jun 17 22:27 ./
drwxr-xr-x. 1 docker docker 4096 Jun 17 21:41 ../
-rw-r--r--. 1 root   root     40 Jun 17 22:47 dades.txt
docker@minikube:~/pod-volume$ cat dades.txt 
aquest es el debian
aquest es el fedora
```

# Conclusions

Kubernetes demostra ser un orquestrador molt bo i útil per als nostres clústers, a més de la gran comunitat que té i les grans empreses que el fan servir.

Sens dubte, aprendre a fer-lo servir és una inversió de cara al present i al futur.

# Bibliografia

* [What is Kubernetes? - Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)

* [Learn Kubernetes Basics - Kubernetes](https://kubernetes.io/docs/tutorials/kubernetes-basics/)

* [Curs Introduction to Kubernetes](https://www.edx.org/course/introduction-to-kubernetes)

* [Documentació Minikube](https://minikube.sigs.k8s.io/docs/start/)

* [Documentació Minikube instal·lació amb Docker](https://minikube.sigs.k8s.io/docs/drivers/docker/)

* [Install and Set Up kubectl - Kubernetes](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux)

* [Documentació Kubernetes instal·lació Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)

* [Documentació ordre kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)

* [Web IU Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)

* [Controlador d'admissions habilitats per defecte](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#which-plugins-are-enabled-by-default)

* [Documentació volums a Kubernetes (oficial)](https://kubernetes.io/docs/concepts/storage/volumes/)

* [Documentació volums persistents (oficial)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)

* [Documentació de PersistentVolumeClaims (oficial)](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)
