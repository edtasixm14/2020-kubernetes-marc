# Què és un contàiner?

Els **contàiners** són (mètodes) centrats en les aplicacions per a una resposta d'alt rendiment, aplicacions escalables...

Són perfectes per l'execució de microserveis per la seva portabilitat, l'execució en entorns virtuals aïllats sense que la seva execució es vegi interferida per altres aplicacions executant-se.

Els **microserveis** són aplicacions lleugeres (fetes en varis llengüatges) amb unes dependències, llibreries i entorn necessàris. Per a assegurar-nos de que s'executaràn de forma correcte les encapsulem junt amb les seves dependències.

Els **contàiners** encapsulen aquests microserveis i les seves dependències, però **no** els executen. Per a això tenim les **imatges de contàiners**.

Les **imatges de contàiners** empaqueten l'aplicació junt amb la seva execució i dependències necessàries, i amb el contàiner llençem aquesta imatge.

# Què és un Container Orchestrator?

El desplegament de contàiners en una sola màquina ens pot resultar útil per fer proves o veure com funciona els serveis que tenim, però això deixa de ser òptim quan passem a un nivell de producció on necessitem vàries (o moltes) màquines oferint el mateix servei. Tot això necessita els següents requeriments:

* Tolerància als errors/caigudes

* Escalabilitat segons la demanda

* Nivell òptim d'usatge dels recursos

* Que els contàiners es puguin comunicar de forma automàtica entre ells

* Accessibilitat des de qualsevol lloc del món

* Capacitat d'actualitzacions/rollbacks sense apagar el servei.

Per a controlar i monitoritzar això fem servir els anomenats **orquestradors**.

Un **Container Orchestrator** (*orquestrador de contàiners*)  és una eina on podem organitzar, monitoritzar i gestionar (entre altres) els nostres contàiners.

Tot i que podem fer un manteniment manual d'un conjunt de contàiners, o utilitzar scripts, la veritat és que els orquestradors facilitzen molt aquestes accions, especialment quan s'han de gestionar cents o milers de contàiners executant-se en una infraestructura global.

La majoria d'orquestradors poden:

* Agrupació de hosts alhora que crean un cluster.

* Programar l'execució de contàiners en hosts segons els recursos disponibles.

* Habilitar que els contàiners d'un cluster puguin comunicar-se entre ells independenment del host on estiguin desplegats.

* Lligar els contàiners i els recursos d'emmagatzematge.

* Agrupar sets de contàiners similars i lligar-los a construccions balançejadores de càrrega per a aplicacions/serveis.

* Administrar i optimitzar l'usatge dels recursos.

* Habilitar l'implementació de polítiques d'accés a les aplicacions/serveis executant-se als contàiners.

Alguns exemples d'orquestradors són:

* `Amazon Elastic Container Service (ECS)`: servei hostejat per **Amazon Web Services (AWS)** per a l'execució de contàiners escalats a la seva infraestructura.

* `Azure Container Instances (ACI)`: Orquestrador bàsic proveït per **Microsoft Azure**.

* `Azure Service Fabric`: Orquestrador open source proveït per **Microsoft Azure**.

* `Kubernetes`: Orquestrador open source, iniciat per Google, part del projecte **Cloud Native Computing Foundation (CNCF)**.

* `Marathon`: Framework per executar contàiners en escala a **Apache Mesos**.

* `Nomad`: Orquestrador proveït per **HashiCorp**.

* `Docker Swarm`: Orquestrador proveït per **Docker, Inc**. Format part del **Docker Engine**.
