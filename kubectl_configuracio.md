Mentres engegem Minikube el procés de startup crea, per defecte, un fitxer *config* dins del directori ***.kube*** del home de l'usuari (referenciat també com a ***dot-kube-config***). 

* Hem de tindre en compte que altres eines no creen aquest fitxer de forma automàtica i l'hem de crear nosaltres de forma manual. També que potser necessitem reconfigurar per adaptar-se a diverses xarxes i clients/server setups.

Aquest fitxer de configuració conté tots els detalls de la connexió requerits per *kubectl*. Per defecte, el binari de *kubectl* analitza aquest fitxer per trobar la connexió del node mestre i les credencials.

Podem veure el contingut d'aquest fitxer al inspeccionar-lo directament o amb la següent ordre:

```bash
# Localització fitxer
[marc@localhost kubernetes-project]$ ll ~/.kube/config 
-rw-------. 1 marc marc 450 may 19 18:24 /home/marc/.kube/config
# Ordre
[marc@localhost kubernetes-project]$ kubectl config view
apiVersion: v1
clusters:
- cluster:
    certificate-authority: /home/marc/.minikube/ca.crt
    server: https://172.17.0.2:8443
  name: minikube
contexts:
- context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
- name: minikube
  user:
    client-certificate: /home/marc/.minikube/profiles/minikube/client.crt
    client-key: /home/marc/.minikube/profiles/minikube/client.key
```

Podem obtindre informació del clúster amb l'ordre `kubectl cluster-info`:

```bash
[marc@localhost kubernetes-project]$ kubectl cluster-info
Kubernetes master is running at https://172.17.0.2:8443
KubeDNS is running at https://172.17.0.2:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```
